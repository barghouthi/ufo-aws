#ifndef _UFO_SMART_EXPR_SET__HPP_
#define _UFO_SMART_EXPR_SET__HPP_

#include "ExprZ3.hpp"
#include "ufo/Expr.hpp"

#include <boost/smart_ptr/scoped_ptr.hpp>

namespace ufo
{

  template<typename SMT>
  class SmartExprSet
  {
    typedef boost::scoped_ptr<SMT> SmtPtr;
  
    ExprFactory& efac;

    SmtPtr smt;  
    ExprVector exprs;
  
  public:
    /** returns true iff e is added to exprs
     */
    bool add(Expr e)
    {
      Stats::resume("SmartExprSet.add");
    
      smt->push();
      smt->assertExpr(e);

      if (smt->solve())
	{
	  //SAT: not subsumed
	  smt->pop();
	  smt->assertExpr(mk<NEG>(e));

	  exprs.push_back (e);
	  Stats::stop("SmartExprSet.add");
	  return true;
	}
      
      //UNSAT: subsumed
      smt->pop();
      Stats::stop("SmartExprSet.add");
      return false;
    }

    /** returns the expr */
    Expr get ()
    {
      return mknary<OR>(mk<FALSE> (efac), exprs.begin(), exprs.end());
    }
  
    void simplify() { return; }
  
    void reset()
    {
      exprs.clear();
      SmtPtr newPtr (new SMT (efac));
      smt.swap (newPtr);    
    }
  
    SmartExprSet(ExprFactory& efac_): efac(efac_), smt (new SMT (efac)) {}
  };
}

#endif
