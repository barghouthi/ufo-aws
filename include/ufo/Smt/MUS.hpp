#ifndef __MUS__HPP_
#define __MUS__HPP_

#include <boost/range.hpp>

#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"

/** Algorithms to comute Minimal Unsatisfiable Subset (MUS) */
namespace ufo
{

  namespace
  {
    /** MUS based on Bradley's implicate/min algorithm from FSIS: 
     * 
     *  Aaron R. Bradley, Zohar Manna: Checking Safety by Inductive
     *  Generalization of Counterexamples to Induction. FMCAD 2007:
     *  173-180.
     */
    template <typename SMT>
    class MusBradley
    {
      SMT &smt;
      /** current set of assumptions */
      ExprVector assumptions;

      template <typename Range>
      MusBradley (SMT &solver, 
		  Range &core) : smt(solver), 
				 assumptions (begin (core), end (core)) 
      {}
  
      template <typename OutputIterator> 
      tribool run (OutputIterator &out)
      {
	return mus (assumptions.size (), out);
      }

      template <typename OutputIterator>
      tribool mus (size_t k, OutputIterator &out)
      {
	if (k == 0 || assumptions.size () == 0) return false;
      
	size_t sz = assumptions.size ();
	size_t lk = k < sz ? k : sz;
    
	ExprVector used;
	tribool res;

	if (lk == sz) res = smt.solve ();
	else
	  res = smt.solveAssuming 
	    (make_iterator_range (assumptions, 0, -lk),
	     used.begin ());
      
	if (!res)
	  {
	    assumptions.clear ();
	    std::copy (used.begin (), used.end (), assumptions.begin ());
	  }
	else if (res)
	  {
	    if (lk == 1)
	      {
		Expr x = assumptions.back ();
		smt.assertExpr (x);
		*(out++) = x;
		assumptions.pop_back ();
	      }
	    else
	      {
		// -- nk = ceil (lk / 2)
		size_t nk = (lk >> 1) + (lk & 1);
	    
		// -- do first half of elements. As a side-effect, nk
		// -- elements from the assumptions vector are removed
		res = mus (nk, out);
		// -- abort if see an unexpected answer
		if (res == boost::indeterminate) return res;
		// -- do the second half of elements
		return mus (lk - nk, out);
	      }
	  }

	return res; 
      }
    };

    struct AsmPredicate
    {
      bool operator() (Expr v) const { return isOpX<ASM> (v); }
    };
  }

  /** Computes Minimal Unsatisfiable Subset. 
   ** 
   ** Given
   **  smt          -- an unsatisfiable SMT solver context with assumptions
   **  assumptions  -- the assumptions to be minimized
   **  out          -- the place to store the output
   ** Returns 
   **  false if the context is unsatisfiable and the MUS is stored in out
   **  true  if the context is satisfiable (and there is no MUS)
   **  indeterminate if SMT solver returned neither true/false on a query
   */
  template <typename SMT, typename Range, typename OutputIterator>
  tribool mus (SMT &smt, Range assumptions, OutputIterator out)
  {
    MusBradley<SMT> mb (smt, assumptions);
    return mb.run (out);
  }


  template <typename SMT, typename OutputIterator>
  tribool mus (SMT &smt, Expr phi, OutputIterator out)
  {
    // -- assert the formula
    smt.assertExpr (phi);

    // -- collect assumptions
    ExprVector orig;
    filter (phi, AsmPredicate (), std::back_inserter (orig));

    // -- solve
    ExprSet used;
    tribool res = smt.solveAssuming (orig, std::inserter (used, used.begin ()));
  
  
    // -- if result is not UNSAT, return
    if (res) return res;
    if (res == indeterminate) return res;
  
    // -- result is UNSAT

    // -- clear assumptions that are not needed
    foreach (Expr a, orig)
      // -- force unused assumption to be false
      if (used.count (a) <= 0) smt.assertExpr (boolop::lneg (a));

    // -- minimize core
    return mus (smt, used, out);
  }

  /**
     mus_basic calls solveAssuming max times and returns the resulting
     set of assumptions.

     If max == 0 (default), it keeps calling solveAssuming until a MUS
     is found.
  */
  template <typename SMT, typename OutputIterator>
  tribool mus_basic (SMT &smt, Expr phi, OutputIterator out, int max = 0)
  {
    // -- assert the formula
    smt.assertExpr (phi);

    // -- collect assumptions
    ExprVector orig;
    filter (phi, AsmPredicate (), std::back_inserter (orig));

    int count = 0;
    ExprSet used;
  
    do {
      used.clear();

      Stopwatch sw;
      // -- solve
      smt.push ();
      tribool res = 
	smt.solveAssuming (orig, std::inserter (used, used.begin ()));
      smt.pop ();
      sw.stop ();
      
      // -- if result is not UNSAT, return
      if (res) return res;
      if (res == indeterminate) return res;
    
      // -- result is UNSAT

      errs () << "MUS basic: cnt: " << count 
	      << " orig: " << orig.size () 
	      << " used: " << used.size () 
	      << " time: " << sw
	      << "\n";

      // -- if number of assumptions didn't decrease, we have found a MUS
      if (orig.size() == used.size()) break;

      // -- clear assumptions that are not needed
      foreach (Expr a, orig)
	// -- force unused assumption to be false
	if (used.count (a) <= 0) smt.assertExpr (boolop::lneg (a));

      orig.clear();
      std::copy (used.begin(), used.end(), std::back_inserter (orig));

      count++;
      //-- exit if count == max
      //-- in case max == 0, exit only when a MUS is found
    } while (max == 0 || count < max);

    // -- add used assumptions to out
    foreach (Expr u, used) *(out++) = u;

    return false;
  }
}


#endif
