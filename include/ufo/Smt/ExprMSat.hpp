#ifndef __EXPR_MSAT_HPP_
#define __EXPR_MSAT_HPP_

//#define MSAT_DEBUG_NAMES 1
#include <map>

#include "ufo/ufo.hpp"
#include "ufo/Arg.hpp"
#include "ufo/Smt/msat.hpp"


using namespace msat;
using namespace llvm;


namespace ufo
{
  namespace detail
  {
    struct TerminalAssert
    {
      template <typename emap_type, typename mmap_type>
      static msat_term marshal (msat_env env, 
				Expr e,
				emap_type &eMap,
				mmap_type &mMap)
      {
	errs () << "Problem: " << *e << "\n";
	assert (0 && "Unreachable");
	exit (1);
      }
    
      template <typename mmap_type>
      static Expr unmarshal (msat_env env,
	                     msat_term m, 
			     ExprFactory &efac,
			     mmap_type &mMap)
      {
	assert (0 && "Unreachable");
	exit (1);
      }
    };    
			       
    /** Handle UFO-specific terminals */
    /*template <typename EC>
    struct TerminalUfoUF
    {
      static msat_term marshal (msat_env env, 
				Expr e,
				emap_type &eMap,
				mmap_type &mMap)
      {
          if (isOpX<VARIANT>(e))
	  {
	    msat_term res;
	    int num = variant::variantNum(e);
	    Expr var = variant::mainVariant(e);
	    std::string svar;
	    svar = "v" + 
	      boost::lexical_cast<std::string,void*>(var.get()) +
	      "_" + boost::lexical_cast<std::string>(num);

                        
            msat_type btype = msat_get_bool_type(env);
	    
	    msat_decl mdecl;
	    
	    if (isOpX<BB> (var))
	      {
		mdecl = msat_declare_function(env, svar.c_str(), btype);
		assert(!MSAT_ERROR_DECL(mdecl));
	        res = msat_make_constant (env, mdecl);
		assert(!MSAT_ERROR_TERM(res));
	      }
	    else
	      {
		Expr exp = var;
		  
		if (isOp<VARIANT>(var)) exp = variant::mainVariant(var);
		  
		const Value* c = 
		  dynamic_cast<const VALUE&> (exp->op ()).get ();
		
		if (dict::valToNum.count(e) <= 0)
		  dict::valToNum[e] = dict::varCount++;
		assert (c != NULL && "can't be null");
                
		if (isBoolType (c->getType ())){
		  msat_term args[1];
		  args[0] = msat_make_number(env, 
		      boost::lexical_cast<std::string>(dict::valToNum[e]).c_str());
		  res = msat_make_uf(env, dict::f_bool, args);
	          assert (!MSAT_ERROR_TERM(res));	  
		}
		else{
                  msat_term args[1];
		  args[0] = msat_make_number(env, 
		      (boost::lexical_cast<std::string>(dict::valToNum[e]) + ".0").c_str());

		  assert(!MSAT_ERROR_DECL(dict::f_real));
		  res = msat_make_uf(env, dict::f_real, args);
		  assert (!MSAT_ERROR_TERM(res));	  
		}

	      }
	      
	    // -- update reverse lookup map
	    mMap [msat_term_id (res)] = e;
	    // -- update forward lookup map
	    eMap[e] = res;
	    return res;
	  }

	if (isOpX<CONST_INT>(e))
	  {
	    const ConstantInt* c = 
	      dynamic_cast<const CONST_INT&> (e->op ()).get ();
	      
	    return 
	      msat_make_number(env, 
			       c->getValue().toString(10, true).c_str());
	  }

	// -- this checks if the variable is boolean
	// -- added for boolean abstraction (AllSat)
	if (isOpX<STRING>(e))
	  {
      //cout << "STRING" << endl;
	    std::string svar = boost::lexical_cast<std::string>(e.get());
	  
      if (svar[0] == 'x')
      {
        //cout << "I'm in." << endl;

        msat_type itype = msat_get_integer_type(env);
        msat_decl mdecl = msat_declare_function(env, svar.c_str(), itype);
        assert(!MSAT_ERROR_DECL(mdecl));
            
        msat_term res = msat_make_constant (env, mdecl);
        assert(!MSAT_ERROR_TERM(res));
        eMap[e] = res;
        mMap[msat_term_id(res)] = e;

        return res;
      }

	    if (svar[0] == 'b')
	      {
                msat_type btype = msat_get_bool_type(env);
		msat_decl mdecl = 
		  msat_declare_function(env, svar.c_str(), btype);
	      
		assert(!MSAT_ERROR_DECL(mdecl));
		msat_term res = msat_make_constant (env, mdecl);
		assert(!MSAT_ERROR_TERM(res));
		eMap[e] = res;
		mMap[msat_term_id(res)] = e;
		return res;
	      }
	    return EC::marshal (env, e, eMap, mMap);
	  }

	if (isOpX<VALUE>(e))
	  {
     //cout << "VALUE" << endl;

	    std::string svar = "x" + 
	      boost::lexical_cast<std::string,void*>(e.get());
	      
	    const Value* c = 
	      dynamic_cast<const VALUE&> (e->op ()).get ();

	    assert (c != NULL && "can't be null");
	  
            msat_type btype = msat_get_bool_type(env);
            msat_type rtype = msat_get_rational_type(env);
	   	    msat_decl mdecl = msat_declare_function (env, svar.c_str(), 
						     isBoolType (c->getType ()) 
						     ? btype : rtype);

	    msat_term res = msat_make_constant (env, mdecl);
	    assert(!MSAT_ERROR_TERM(res));
	    eMap[e] = res;
	    mMap[msat_term_id(res)] = e;
	    return res;
	  }


	if (isOpX<BB>(e))
	  {
	    std::string svar = "BB" + 
	      boost::lexical_cast<std::string,void*>(e.get());
            msat_type btype = msat_get_bool_type(env);
	    msat_decl mdecl = 
	      msat_declare_function(env, svar.c_str(), btype);
	    assert(!MSAT_ERROR_DECL(mdecl));
	    msat_term res = msat_make_constant (env, mdecl);
	    assert(!MSAT_ERROR_TERM(res));
	    eMap[e] = res;
	    mMap[msat_term_id(res)] = e;
	    return res;
	  }

	if (isOpX<NODE_PAIR> (e)){
	  const std::pair<Node*, Node*> np = dynamic_cast<const NODE_PAIR&>(e->op()).get();
	  std::string svar = "NP(" + boost::lexical_cast<std::string>(np.first) + "," + 
	                             boost::lexical_cast<std::string>(np.second) + ")";
          msat_type btype = msat_get_bool_type(env);
	  msat_decl mdecl = 
	      msat_declare_function(env, svar.c_str(), btype);
	  assert(!MSAT_ERROR_DECL(mdecl));
	  msat_term res = msat_make_constant (env, mdecl);
	  assert(!MSAT_ERROR_TERM(res));
	  eMap[e] = res;
	  mMap[msat_term_id(res)] = e;
	  return res;
	}
	
	if (isOpX<ASSUMPTION> (e)){
	  const Assumption np = dynamic_cast<const ASSUMPTION&>(e->op()).get();
	  std::string svar = "NP(" + boost::lexical_cast<std::string>(np.p.first) + "," + 
	                             boost::lexical_cast<std::string>(np.p.second) + ")";
	  
	  
	  msat_type btype = msat_get_bool_type(env);
	  msat_decl mdecl = 
	      msat_declare_function(env, svar.c_str(), btype);
	  assert(!MSAT_ERROR_DECL(mdecl));
	  msat_term res = msat_make_constant (env, mdecl);
	  assert(!MSAT_ERROR_TERM(res));
	  eMap[e] = res;
	  mMap[msat_term_id(res)] = e;
	  return res;
	}
	

	if (isOpX<NODE> (e))
	  {
	    std::string svar = "N" + 
	      boost::lexical_cast<std::string,void*>(e.get());
            msat_type btype = msat_get_bool_type(env);
	    msat_decl mdecl = 
	      msat_declare_function(env, svar.c_str(), btype);
	    assert(!MSAT_ERROR_DECL(mdecl));
	    msat_term res = msat_make_constant (env, mdecl);
	    assert(!MSAT_ERROR_TERM(res));
	    // no need to update reverse lookup map
	    eMap[e] = res;
	    mMap[msat_term_id(res)] = e;	      
	    return res;
	  }

	return EC::marshal (env, e, eMap, mMap);
      }

      static Expr unmarshal (msat_env env,
	                     msat_term m, 
			     ExprFactory &efac,
			     mmap_type &mMap)
      {
	std::map<int,Expr>::const_iterator it = mMap.find (msat_term_id (m));
	if (it != mMap.end ()) {
	  return it->second;
	}


	return EC::unmarshal (env, m, efac, mMap);
      }
      }; */


    /** Handle UFO-specific terminals */
    template <typename EC>
    struct TerminalUfo
    {
      template <typename emap_type, typename mmap_type>
      static msat_term marshal (msat_env env, 
				Expr e,
				emap_type &eMap,
				mmap_type &mMap)
      {
	if (isOpX<VARIANT>(e))
	  {
	    int num = variant::variantNum(e);
	    Expr var = variant::mainVariant(e);
	    std::string svar;
	    svar = "v" + 
	      boost::lexical_cast<std::string,void*>(var.get()) +
	      "_" + boost::lexical_cast<std::string>(num);
#ifdef MSAT_DEBUG_NAMES	    
	    svar = boost::lexical_cast<std::string>(*var) + 
	      "_" + boost::lexical_cast<std::string>(num);
#endif


                        
            msat_type btype = msat_get_bool_type(env);
            msat_type rtype = 
	      ufocl::USE_INTS ? msat_get_integer_type (env) : 
	      msat_get_rational_type(env);
	    msat_decl mdecl;
	    if (isOpX<BB> (var))
	      {
		mdecl = msat_declare_function(env, svar.c_str(), btype);
		assert(!MSAT_ERROR_DECL(mdecl));
	      }
	    else
	      {
		Expr exp = var;
		  
		if (isOp<VARIANT>(var)) exp = variant::mainVariant(var);
		  
		const Value* c = getTerm<const Value*> (exp);
		  
		assert (c != NULL && "can't be null");
 
		mdecl = msat_declare_function
		  (env, svar.c_str(), 
		   isBoolType (c->getType ()) ? btype : rtype);
		assert(!MSAT_ERROR_DECL(mdecl));
	      }
	      
	    msat_term res = msat_make_constant (env, mdecl);
            assert(!MSAT_ERROR_TERM(res));
	    // -- update reverse lookup map
	    mMap [msat_term_id (res)] = e;
	    // -- update forward lookup map
	    eMap[e] = res;
	    return res;
	  }

	// if (isOpX<CONST_INT>(e))
	//   {
	//     const ConstantInt* c = 
	//       dynamic_cast<const CONST_INT&> (e->op ()).get ();
	      
	//     return 
	//       msat_make_number(env, 
	// 		       c->getValue().toString(10, true).c_str());
	//   }

	// -- this checks if the variable is boolean
	// -- added for boolean abstraction (AllSat)
	if (isOpX<STRING>(e))
	  {
      //cout << "STRING" << endl;
	    std::string svar = boost::lexical_cast<std::string>(e.get());
	  
      if (svar[0] == 'x')
      {
        //cout << "I'm in." << endl;

        msat_type itype = msat_get_integer_type(env);
        msat_decl mdecl = msat_declare_function(env, svar.c_str(), itype);
        assert(!MSAT_ERROR_DECL(mdecl));
            
        msat_term res = msat_make_constant (env, mdecl);
        assert(!MSAT_ERROR_TERM(res));
        eMap[e] = res;
        mMap[msat_term_id(res)] = e;

        return res;
      }

	    if (svar[0] == 'b')
	      {
                msat_type btype = msat_get_bool_type(env);
		msat_decl mdecl = 
		  msat_declare_function(env, svar.c_str(), btype);
	      
		assert(!MSAT_ERROR_DECL(mdecl));
		msat_term res = msat_make_constant (env, mdecl);
		assert(!MSAT_ERROR_TERM(res));
		eMap[e] = res;
		mMap[msat_term_id(res)] = e;
		return res;
	      }
	    return EC::marshal (env, e, eMap, mMap);
	  }

	if (isOpX<VALUE>(e))
	  {
     //cout << "VALUE" << endl;

	    std::string svar = "x" + 
	      boost::lexical_cast<std::string,void*>(e.get());
	      
	    const Value* c = getTerm<const Value*> (e);

	    assert (c != NULL && "can't be null");
	  
            msat_type btype = msat_get_bool_type(env);
            msat_type rtype = ufocl::USE_INTS ? 
	      msat_get_integer_type (env) : msat_get_rational_type(env);
	    msat_decl mdecl = msat_declare_function (env, svar.c_str(), 
						     isBoolType (c->getType ()) 
						     ? btype : rtype);

	    msat_term res = msat_make_constant (env, mdecl);
	    assert(!MSAT_ERROR_TERM(res));
	    eMap[e] = res;
	    mMap[msat_term_id(res)] = e;
	    return res;
	  }


	if (isOpX<BB>(e))
	  {
	    std::string svar = "BB" + 
	      boost::lexical_cast<std::string,void*>(e.get());
            msat_type btype = msat_get_bool_type(env);
	    msat_decl mdecl = 
	      msat_declare_function(env, svar.c_str(), btype);
	    assert(!MSAT_ERROR_DECL(mdecl));
	    msat_term res = msat_make_constant (env, mdecl);
	    assert(!MSAT_ERROR_TERM(res));
	    eMap[e] = res;
	    mMap[msat_term_id(res)] = e;
	    return res;
	  }

	if (isOpX<NODE> (e))
	  {
	    std::string svar = "N" + 
	      boost::lexical_cast<std::string,void*>(e.get());
            msat_type btype = msat_get_bool_type(env);
	    msat_decl mdecl = 
	      msat_declare_function(env, svar.c_str(), btype);
	    assert(!MSAT_ERROR_DECL(mdecl));
	    msat_term res = msat_make_constant (env, mdecl);
	    assert(!MSAT_ERROR_TERM(res));
	    // no need to update reverse lookup map
	    eMap[e] = res;
	    mMap[msat_term_id(res)] = e;	      
	    return res;
	  }

	// if (isOpX<NODE_PAIR> (e)){
	//   const std::pair<Node*, Node*> np = dynamic_cast<const NODE_PAIR&>(e->op()).get();
	//   std::string svar = "NP(" + boost::lexical_cast<std::string>(np.first) + "," + 
	//                              boost::lexical_cast<std::string>(np.second) + ")";
	  
	//   msat_type btype = msat_get_bool_type(env);
	//   msat_decl mdecl = 
	//       msat_declare_function(env, svar.c_str(), btype);
	//   assert(!MSAT_ERROR_DECL(mdecl));
	//   msat_term res = msat_make_constant (env, mdecl);
	//   assert(!MSAT_ERROR_TERM(res));
	//   eMap[e] = res;
	//   mMap[msat_term_id(res)] = e;
	//   return res;
	// }
	
	// if (isOpX<ASSUMPTION> (e)){
	//   const Assumption np = dynamic_cast<const ASSUMPTION&>(e->op()).get();
	//   std::string svar = "ASM(" + boost::lexical_cast<std::string>(np.p.first) + "," + 
	//                              boost::lexical_cast<std::string>(np.p.second) + ")";
          
	//   msat_type btype = msat_get_bool_type(env);
	//   msat_decl mdecl = 
	//       msat_declare_function(env, svar.c_str(), btype);
	//   assert(!MSAT_ERROR_DECL(mdecl));
	//   msat_term res = msat_make_constant (env, mdecl);
	//   assert(!MSAT_ERROR_TERM(res));
	//   eMap[e] = res;
	//   mMap[msat_term_id(res)] = e;
	//   return res;
	// }
	

	return EC::marshal (env, e, eMap, mMap);
      }
    
      template <typename mmap_type>
      static Expr unmarshal (msat_env env,
	                     msat_term m, 
			     ExprFactory &efac,
			     mmap_type &mMap)
      {
	typedef typename mmap_type::const_iterator mmap_const_iterator;
	mmap_const_iterator it = mMap.find (msat_term_id (m));
	if (it != mMap.end ()) return it->second;

	return EC::unmarshal (env, m, efac, mMap);
      }
    
    };
  

    /** Basic Boolean and Numeric expressions*/
    template <typename EC> 
    struct BasicExprConverter
    {
      template <typename emap_type, typename mmap_type>
      static msat_term marshal (msat_env env, 
				Expr e,
				emap_type &eMap,
				mmap_type &mMap)
      {
	assert(e);
	if (isOpX<TRUE>(e)) return msat_make_true (env);
	if (isOpX<FALSE>(e)) return msat_make_false (env);
      
	if (isOpX<INT>(e))
	  return 
	    msat_make_number(env, 
			     boost::lexical_cast<std::string>
			     (e.get()).c_str());

	if (isOpX<MPQ>(e))
	  {
	    const MPQ& op = dynamic_cast<const MPQ&>(e->op ());
	    return msat_make_number 
	      (env, boost::lexical_cast<std::string> (op.get()).c_str ());
	  }
	if (isOpX<MPZ>(e))
	  {
	    const MPZ& op = dynamic_cast<const MPZ&>(e->op ());
	    return msat_make_number 
	      (env, boost::lexical_cast<std::string> (op.get()).c_str ());
	  }

	
	typedef typename emap_type::const_iterator emap_const_iterator;
	emap_const_iterator it = eMap.find (e);
	if (it != eMap.end ()) return it->second;
      
	int arity = e->arity ();
	/** other terminal expressions */
	if (arity == 0) return EC::marshal (env, e, eMap, mMap);
	
	msat_term res;
	// -- clear up result (for asserts later)
	MSAT_MAKE_ERROR_TERM(res);


	if (arity == 2 && bind::isBoolVar (e))
	  {
	    Expr edge = bind::name (e);    
	    string svar = "E" + boost::lexical_cast<std::string> (edge);

	    msat_type btype = msat_get_bool_type(env);
	    msat_decl mdecl = msat_declare_function (env, svar.c_str(), btype);
	    assert(!MSAT_ERROR_DECL(mdecl));
	  
	    res = msat_make_constant (env, mdecl);
	    // -- inverse map
	    mMap [msat_term_id (res)] = e;
	  }
	else if (arity == 1)
	  {
	    //then it's a NEG or UN_MINUS or ABS
	    //XXX not sure how to handle ABS in mathsat
	    if (isOpX<UN_MINUS>(e))
	      assert (0);
	      //res = msat_make_negate(env, marshal (env, e->left(), eMap, mMap));
	    else if (isOpX<NEG>(e))
	      res = msat_make_not(env, marshal (env, e->left(), eMap, mMap));
	    else
	      return EC::marshal (env, e, eMap, mMap);
	  }
	else if (arity == 2)
	  {

	    msat_term t1 = marshal(env, e->left(), eMap, mMap);
	    msat_term t2 = marshal(env, e->right(), eMap, mMap);

	    /** BoolOp */
	    if (isOpX<AND>(e)) 
	      res = msat_make_and(env, t1, t2);
	    else if (isOpX<IMPL>(e))
	      res = msat_make_or(env, msat_make_not(env, t1), t2);
	    else if (isOpX<OR>(e))
	      res = msat_make_or(env, t1, t2);
	    else if (isOpX<IFF>(e))
	      res = msat_make_iff(env, t1, t2);
	    else if (isOpX<XOR>(e))
	      res = msat_make_not(env, msat_make_iff(env, t1, t2));

	    /** NumericOp */
	    else if (isOpX<PLUS>(e))
	      res = msat_make_plus(env, t1, t2);
	    else if (isOpX<MINUS>(e)){
	      res = 
		msat_make_plus(env, t1, 
			       msat_make_times(env, 
					       msat_make_number(env, "-1"), 
					       t2));
	    }
	    else if (isOpX<MULT>(e))
	      res = msat_make_times(env, t1, t2);
	    else if (isOpX<DIV>(e)){
	      assert (0 && "division not supported");
	      //res = msat_make_divide(env, t1, t2);
	    }
	    /** Comparisson Op */
	    else if (isOpX<EQ>(e))
	      res = msat_make_equal(env, t1, t2);
	    else if (isOpX<NEQ>(e))
	      res = msat_make_not(env, msat_make_equal(env, t1, t2));
	    else if (isOpX<LEQ>(e))
	      res =  msat_make_leq(env, t1, t2);
	    
	    else if (isOpX<GEQ>(e))
	      res = msat_make_leq(env, t2, t1);
	    else if (isOpX<LT>(e))
	      res = msat_make_not(env, msat_make_leq(env, t2, t1));
	    else if (isOpX<GT>(e))
	      res = msat_make_not(env, msat_make_leq(env, t1, t2));
	    else
	      return EC::marshal (env, e, eMap, mMap);
	  }
	else if (arity == 3 && isOpX<ITE> (e))
	  {
	    res = 
	      msat_make_term_ite (env,
	    			  marshal (env, (*e)[0], eMap, mMap),
	    			  marshal (env, (*e)[1], eMap, mMap),
	    			  marshal (env, (*e)[2], eMap, mMap));	    
	  }
	
	else
	
	  // XXX it must be AND or OR nary expression
	  // XXX because we can't get nary + or - from llvm
	  // XXX and msat doesn't return nary terms??
	
	  if (isOpX<AND> (e) || isOpX<OR> (e) || isOpX<XOR> (e))
	    {

	      std::vector<msat_term> args;
      
	      for (ENode::args_iterator it = e->args_begin(), 
		     end = e->args_end(); it != end; ++it)
		args.push_back (marshal (env, *it, eMap, mMap));
          
	      if (isOpX<AND>(e))
		{
		  res = args [0];
		  for (size_t i = 1; i < args.size(); ++i)
		    res = msat_make_and(env, res, args[i]);
		}
	      else if (isOpX<OR>(e))
		{
		  res = args[0];
		  for (size_t i=1; i < args.size(); ++i)
		    res = msat_make_or(env, res, args[i]);
		}
	      else if (isOpX<XOR>(e))
		{
		  assert(args.size() == 2);
		  res = args[0];
		  for (size_t i=1; i < args.size(); ++i)
		    res = msat_make_not(env, 
					msat_make_iff(env, args[0], args[1]));
		} 
	    }
	  else
	    return EC::marshal (env, e, eMap, mMap);
      
	if (MSAT_ERROR_TERM(res))
	  {
	    errs () << "Error on expression of arity " << arity << "\n"
		    << "EXPR is " << boost::lexical_cast<std::string>(*e) 
		    << "\n"
		    << "MSAT: " << msat_last_error_message (env)
		    << "\n";
	  }
	
	assert (!MSAT_ERROR_TERM(res)); // one of the above conditions held
	eMap [e] = res;
	//mMap [msat_term_id (res)] = e;
      
	return res;
      }
    
      template <typename mmap_type>
      static Expr unmarshal (msat_env env,
	                     msat_term m, 
			     ExprFactory &efac,
			     mmap_type &mMap)
      {
	// -- null term is return by msat_get_model_value to indicate
	// -- unconstraint model variable
	if (m.repr == NULL) return mk<NONDET> (efac);
	if (msat_term_is_true(env, m)) return mk<TRUE>(efac);    
	if (msat_term_is_false(env, m)) return mk<FALSE>(efac);
	if (msat_term_is_not (env, m))
	  return mk<NEG> (unmarshal (env, msat_term_get_arg (m, 0), efac, mMap));
	//if (msat_term_is_negate (env, m))
	  //return mk<UN_MINUS> (unmarshal (msat_term_get_arg (m, 0), efac, mMap));
      
	if (msat_term_is_number(env, m))
	  {
	    std::string snum = msat_term_repr (m);
	    const mpq_class mpq = mpq_class(snum);
	    Expr res = mkTerm (mpq, efac);
	    return res;
	  }

	typedef typename mmap_type::const_iterator mmap_const_iterator;
	mmap_const_iterator it = mMap.find (msat_term_id (m));
	if (it != mMap.end ()) return it->second;


	assert(!msat_term_is_uf(env,m) && "couldn't convert uf back to expr\n");

	Expr e;
	std::vector<Expr> args;
      
	for (size_t i = 0; i < (size_t)msat_term_arity (m); i++)
	  args.push_back (unmarshal (env, msat_term_get_arg(m, i), efac, mMap));
      

	if (msat_term_is_and (env, m))
	  e = mknary<AND> (args.begin(), args.end());
	else if (msat_term_is_or (env, m))
	  e =  mknary<OR> (args.begin(), args.end());
	//else if (msat_term_is_xor (env, m))
	  //e = mknary<XOR> (args.begin(), args.end());
	else if (msat_term_is_iff (env, m))
	  e =  mknary<IFF> (args.begin(), args.end());
	//else if (msat_term_is_implies (m))
	  //e =  mknary<IMPL> (args.begin(), args.end());
	else if (msat_term_is_equal (env, m))
	  e =  mknary<EQ> (args.begin(), args.end());
	//else if (msat_term_is_lt (m))
	  //e =  mknary<LT> (args.begin(), args.end());
	//else if (msat_term_is_gt (m))
	  //e =  mknary<GT> (args.begin(), args.end());
	else if (msat_term_is_leq (env, m))
	  e =  mknary<LEQ> (args.begin(), args.end());
	//else if (msat_term_is_geq (m))
	  //e =  mknary<GEQ> (args.begin(), args.end());
	else if (msat_term_is_plus (env, m))
	  e =  mknary<PLUS> (args.begin(), args.end());
	//else if (msat_term_is_minus (m))
	  //e =  mknary<MINUS> (args.begin(), args.end());
	else if (msat_term_is_times (env, m)) 
	  e =  mknary<MULT> (args.begin(), args.end());
	//else if (msat_term_is_bv_sdiv (m) || msat_term_is_bv_udiv (m))
	  //e = mknary<DIV> (args.begin(), args.end());
	//else if (msat_term_is_bv_urem (m) || msat_term_is_bv_srem (m))
	  //e = mknary<MOD> (args.begin(), args.end());
	else
	  return EC::unmarshal (env, m, efac, mMap);

	mMap [msat_term_id (m)] = e;
	return e;
      }		       
    };

    typedef TerminalUfo<TerminalAssert> UFO_TERMINAL;
    typedef BasicExprConverter<UFO_TERMINAL> ExprConverter;
  }
}

namespace ufo 
{
  typedef MSAT<Expr,ufo::detail::ExprConverter> ExprMSat;
}



#endif
