#ifndef __MSAT__HPP_
#define __MSAT__HPP_

#include "mathsat.h"
#include "ufo/ufo.hpp"
#include "boost/iterator/iterator_facade.hpp"

using namespace expr;
using namespace expr::op;

namespace msat
{

  // XXX AG: Cannot define (as opposed to declar) variables in a header file 
  // -- for encoding with uninterpreted functions
  /*  struct dict{
    public:
    static std::map<Expr, int> valToNum;
    static int varCount;

    // -- uf for real program vars
    static msat_decl f_real;

    // -- uf for bool program vars
    static msat_decl f_bool;
  };

  int dict::varCount = 0;
  msat_decl dict::f_real;
  msat_decl dict::f_bool;
  std::map<Expr, int> dict::valToNum; */



  /** parameterized by type T of expressions and a converter EC
      between mathsat expressions and T */
  template <typename T, typename EC> 
  class MSAT
  {
    private:

    typedef MSAT<T,EC> this_type;

    msat_env env;
    ExprFactory& efac;


    // -- map from Expr to msat_term
    //typedef std::map<Expr, msat_term> expr_map_type;
    typedef boost::unordered_map<Expr,msat_term> expr_map_type;
    typedef std::vector<expr_map_type> expr_maps_type;
    expr_maps_type exprMaps;    

    // -- map from msat_term id to Expr
    typedef std::map<int, Expr> msat_map_type;
    typedef std::vector<msat_map_type> msat_maps_type;
    msat_maps_type msatMaps;
    

    
  public:
    class model_iterator : 
      public boost::iterator_facade<model_iterator, 
				    ExprPair, 
				    boost::forward_traversal_tag>
    {
    private:
      MSAT *msat;
      msat_model_iterator msat_iter;

      mutable ExprPair kv;
      
      
    public:
      model_iterator () : msat(0) {}     
      model_iterator (this_type &m, msat_model_iterator iter) : 
	msat (&m), msat_iter (iter) { increment (); }

      model_iterator (const model_iterator &other) :
	msat(other.msat), msat_iter (other.msat_iter), kv(kv) {}
      
      model_iterator &operator= (const model_iterator& other)
      {
	model_iterator tmp(other);
	msat = tmp.msat;
	msat_iter = tmp.msat_iter;
	std::swap (tmp.kv, kv);
	return *this;
      }
      
    private:
      friend class boost::iterator_core_access;
      
      void increment () 
      {
	msat_term key;
	msat_term value;

	key.repr = NULL;
	value.repr = NULL;
	if (msat != NULL && msat_model_iterator_has_next (msat_iter))
	  {
	    msat_model_iterator_next (msat_iter, &key, &value);
	    kv = ExprPair (msat->msat2expr (key), msat->msat2expr (value));
	  }
	
	else
	  // -- mark as past end
	  msat = NULL;
      }

      bool equal (const model_iterator &other) const
      {
	if (other.msat == NULL && msat == NULL) return true;
	if (other.msat == NULL || msat == NULL) return false;
	return msat_iter.repr == other.msat_iter.repr;
      }
      
      ExprPair &dereference () const { return kv; }
    };
    
    
    
    /**
     * MATHSAT Model. Provides begin() and end() methods to iterate
     * over all bindings in the model.
     */
    class MSatModel 
    {
      MSAT *msat;
      msat_model_iterator iter;

    private:
      MSatModel (const MSatModel &other) {}
      
    public :
      MSatModel (MSAT &m) : 
	msat(&m), 
	iter (msat_create_model_iterator (msat->env)) {}      

      ~MSatModel () { msat_destroy_model_iterator (iter); }
      
      typedef model_iterator iterator;
      typedef model_iterator const_iterator;

      
      iterator begin () 
      {
	return iterator (*msat, iter);
      }
      
      iterator end () 
      {
    	return model_iterator ();
      }
    };
      
    friend class model_iterator;
    friend class MSatModel;
    
  private:
    MSAT (const MSAT& other) { assert (0); }
    
  public:
    MSAT(ExprFactory& e): efac(e)
    {
      env.repr = NULL;
      restart (false);
    }

    ~MSAT() { msat_destroy_env(env); }

    /** restarts MATHSAT */
    void restart (bool interp, bool core = false, bool log = false)
    {
      msatMaps.clear ();
      exprMaps.clear ();      
      if (env.repr != NULL) msat_destroy_env (env);

      msat_config cfg = msat_create_config();
      
      // -- set config options
      msat_set_option(cfg, "interpolation", interp ? "true" : "false");
      msat_set_option(cfg, "model_generation", "true");
      msat_set_option(cfg, "unsat_core_generation", core ? "true" : "false");
      
      if (log){
        msat_set_option(cfg, "debug.api_call_trace", "2");
        msat_set_option(cfg, "debug.api_call_trace_filename", "msat.log");
      }

      env = msat_create_env (cfg);
      msatMaps.push_back (msat_map_type ());
      exprMaps.push_back (expr_map_type ());

      reset (false);
    }
  
    /** Resets to default state, while remembering all declarations.
     ** interp==true enables interpolation
     */
    void reset (bool interp)
    {
      msat_reset_env(env);
      //errs () << "Resetting MSAT\n\n";
      
      // -- initialize f_real and f_bool 
      //dict::varCount = 0;
      msat_type param_real[1];
      param_real[0] = msat_get_rational_type(env);
	
      //msat_type t_real = msat_get_function_type(env,  param_real, 1, msat_get_rational_type(env));
      //msat_type t_bool = msat_get_function_type(env,  param_real, 1, msat_get_bool_type(env));
	
      // dict::f_real = msat_declare_function(env, "f_real", t_real);
      // dict::f_bool = msat_declare_function(env, "f_bool", t_bool);
	
      // assert (!MSAT_ERROR_DECL(dict::f_real) && !MSAT_ERROR_DECL(dict::f_bool));
      
 
      //XXX mathsat5 documentation says to add theory
      //XXX but function no longer exists!

      //int check = msat_add_theory(env, MSAT_LRA);
      //assert(check == 0);

      /* check = msat_add_theory (env, MSAT_UF); */
      /* assert (check == 0); */
      /* check = msat_set_theory_combination (env, MSAT_COMB_DTC); */
      

      //if (interp)
	//{
	  //check = msat_init_interpolation(env);
	  //assert(check == 0);
	//}
    }

    /** Pushes current context */
    bool push () 
    { 
      bool res =  (msat_push_backtrack_point(env) == 0); 
      if (res) 
	{
	  exprMaps.push_back (exprMaps.back ());
	  msatMaps.push_back (msatMaps.back ());
	}
      return res;
    }

    /** Pops current context */
    bool pop () 
    { 
      bool res = (msat_pop_backtrack_point(env) == 0); 
      if (res)
	{
	  exprMaps.pop_back ();
	  msatMaps.pop_back ();
	}
      return res;
    }

    Expr simplify (Expr e)
    {
      restart (false);
      return  msat2expr (expr2msat (e));
    }


    /** returns all positive and negative expressions implied by exp */
    template <typename InputIterator, typename OutputIterator>
    void implied (Expr exp, InputIterator beg, InputIterator end, 
		  OutputIterator out)
    {
      restart (false);
      
      assertExpr (exp);

      for (InputIterator it = beg; it != end; ++it)
	{
	  Expr p = *it;

	  push ();
	  assertExpr (p);
	  boost::tribool res = solve ();
	  pop ();

	  if (!res)
	    {
	      *(out++) = mk<NEG> (p);
	      continue;
	    }

	  push ();
	  assertExpr (mk<NEG> (p));
	  res = solve ();
	  pop ();
	  if (!res) *(out++) = p;
	}
    }

    /** Checks satisfiability of a given expression in the current context */
    boost::tribool isSAT (Expr e)
      {
	restart (false);

	push ();
	assert(assertExpr (e));
	boost::tribool res = solve ();
	/*if (false){
	  msat_model_iterator it = 
	    msat_create_model_iterator(env);

	  assert(!MSAT_ERROR_MODEL_ITERATOR(it));
	  while (msat_model_iterator_has_next(it)){
	    msat_term t;
	    msat_term v;
	    msat_model_iterator_next(it, &t, &v);
	    errs () << "MSAT: "<< msat_term_repr(t)
	      << " ==>  " << msat_term_repr(v) << "\n";
	  }
	  msat_destroy_model_iterator(it);
	  }*/
	pop();
	return res;
      }
    
    boost::tribool isSATAssumptions(Expr e, std::vector<Expr>& assumptions, 
	                            std::vector<Expr>& results){
      restart (false,true,true);

      push();

      // -- first assert the formula
      assertExpr(e);

      // -- create assumptions
      msat_term* msatAssumptions = new msat_term [assumptions.size ()];
      
      size_t count = 0;
      foreach (Expr a, assumptions){
	msatAssumptions[count++] = expr2msat(a);
      }

      // -- solve assuming msatAssumptions
      msat_result res = msat_solve_with_assumptions(env, msatAssumptions, assumptions.size());

      
      assert(res != MSAT_UNKNOWN);

      if (res == MSAT_UNSAT){
	errs () << "\nUNSAT with assumptions\n";
	size_t length;
        
	msat_term* trueAssumptions = msat_get_unsat_assumptions(env, &length);
	
	assert(trueAssumptions != NULL);

	for (size_t i = 0; i < length; ++i){
	  results.push_back(msat2expr(trueAssumptions[i]));
	}
	

	msat_free(trueAssumptions);
      }

      free(msatAssumptions);
      
      pop ();

      restart(false,true);
      switch (res)
	{
	case MSAT_SAT:
	  return true;
	case MSAT_UNSAT:
	  return false;
	case MSAT_UNKNOWN:
	  return boost::indeterminate;
	}
      assert(0);
    }
    
    /** Returns all satisfying assignments to important terms */
    Expr allSAT (Expr formula, const ExprVector& important)
    {
      // assert(0);
      // return mk<TRUE>(efac);
      restart(false);
      restart(false);
      push ();

      msat_term mterm = expr2msat (formula);
      msat_term* impTerm = new msat_term [important.size ()];

      for (size_t i = 0; i < important.size(); ++i)
        impTerm[i] = expr2msat(important[i]);
      
      msat_assert_formula (env, mterm);
      
      cbWrapper wrp;
      wrp.ms = this;
      errs () << "msat allsat: ";
      int ret = msat_all_sat(env, impTerm, important.size(), 
			     allsatCallback, &wrp);
      errs () << "\n";
      
      assert (ret != -1);
      // -- msat error
      if (ret == -1) 
	{
	  //errs () << "Boolean pred abs fail -- msat_all_sat error\n";
	  exit (20);
	}
      
     
      Expr res;
      if (ret == (1 << important.size())) res = mk<TRUE>(efac);
      else if (wrp.res.size () == 0) res = mk<FALSE>(efac);
      else if (wrp.res.size () == 1) res = wrp.res [0];
      else res = mknary<OR>(wrp.res.begin (), wrp.res.end ());

      pop();
      delete [] impTerm;

      return res;
    }
    
    
    /** asserts expression in the current context */
    bool assertExpr (Expr e)
    {
      msat_term res = expr2msat (e);
      return msat_assert_formula (env, res) == 0;
    }

    // -- return group name
    int assertAndGroup (Expr e){
      // -- set interpolation group
      int gid = msat_create_itp_group(env);
      assert(gid != -1);

      assert(msat_set_itp_group(env, gid) == 0);

      // -- assert formula
      assert(assertExpr(e));
      return gid;
    }
    
    /** decides current context */
    boost::tribool solve ()
    {
      msat_result res = msat_solve (env);
      switch (res)
	{
	case MSAT_SAT:
	  return true;
	case MSAT_UNSAT:
	  return false;
	case MSAT_UNKNOWN:
	  return boost::indeterminate;
	}
      assert (0 && "Unreachable");
    }
    
    template<typename OutputIterator>
    boost::tribool interpolate(int* groups, size_t size, OutputIterator out){
      boost::tribool res = solve ();
      
      // -- there is no interpolant if
      // -- a) the formula is SAT
      // -- b) the formula is unknown
      // -- c) the single formula is interpolated
      if (res || boost::indeterminate (res))
	{
	  return false;
	}


      for (size_t count = 1; count < size; ++count){
	*(out++) = msat2expr (msat_get_interpolant (env, groups, count));
        errs () << "Computing interpolant for group " << groups[count-1]<<"\n";
      }
      return true;

    }
    
    /** interpolates in the current context */
    template<typename InputIterator, typename OutputIterator>
    boost::tribool interpolate (InputIterator begin, InputIterator end, 
				OutputIterator out)
    {
      restart (true);
      // -- enable interpolation
      reset(true); 


      std::vector<msat_term> mterms;
      for (InputIterator i = begin, e = end; i != e; ++i) 
	mterms.push_back (expr2msat (*i));

      size_t size = (end - begin);      
      int* groups = new int[size];
      for (int i = mterms.size() - 1; i >= 0; --i)
	{
	  groups[i] = msat_create_itp_group(env);
	  msat_set_itp_group (env, groups[i]);
          msat_assert_formula(env, mterms[i]);
	}

      boost::tribool res = solve ();
      
      assert(!boost::indeterminate(res));
      // -- there is no interpolant if
      // -- a) the formula is SAT
      // -- b) the formula is unknown
      // -- c) the single formula is interpolated
      if (res || boost::indeterminate (res))
	{
	  delete [] groups;
	  // if (res)
	  //   {
	  //     msat_model_iterator it = 
	  // 	msat_create_model_iterator(env);

	  //     assert(!MSAT_ERROR_MODEL_ITERATOR(it));
	  //     while (msat_model_iterator_has_next(it)){
	  // 	msat_term t;
	  // 	msat_term v;
	  // 	msat_model_iterator_next(it, &t, &v);
	  // 	errs () << "MSAT: "<< msat_term_repr(t)
	  // 		<< " ==>  " << msat_term_repr(v) << "\n";
	  //     }
	  //     msat_destroy_model_iterator(it);
	  //   }

	  return false;
	}

      for (size_t count = 1; count < size; ++count){
	msat_term interp = msat_get_interpolant (env, groups, count);
	*(out++) = msat2expr (interp);
      }
      
      delete [] groups;
      return true;
    }
    
    /** returns the current model (a pair of iterators) 
     ** XXX This interface is unused and is untested
     */
    MSatModel getModel () { return MSatModel (*this); }
    

    /**
       Returns the value a given expression has in the current model.
       NONDET is returned to indicate the the value is unconstraint
     */
    Expr getModelValue (Expr e)
    {
      return msat2expr (msat_get_model_value (env, expr2msat (e)));
    }

  private:
    msat_term expr2msat (Expr e)
    {
      Stats::resume ("msat.from_expr");
      msat_term res = EC::marshal (env, e, exprMaps.back (), msatMaps.back ());
      Stats::stop ("msat.from_expr");
      return res;
    }
    
    Expr msat2expr (msat_term m)
    {
      Stats::resume ("msat.to_expr");
      Expr res = EC::unmarshal (env, m, efac, msatMaps.back ());
      Stats::stop ("msat.to_expr");
      return res;
    }
    
    
    // msat_term expr2msatold(Expr e)
    // {
    //   //errs() << "expr2msat: " << e <<"\n";
    //   // -- lookup in the cache
    //   expr_map_type::iterator it = exprMap.find(e);
    //   if (it != exprMap.end()){ 
    //     return it->second;
    //   }

    //   // -- First, check if terminal
    //   int arity = e->arity();
    //   if (arity == 0)
    // 	{

    // 	  //then it's a terminal expression
    // 	  //kinds:  constant, true, and false
    // 	  if (isOpX<TRUE>(e)) return msat_make_true(env);
    // 	  if (isOpX<FALSE>(e)) return msat_make_false(env);
    // 	  if (isOpX<CONST_INT>(e))
    // 	    {
    // 	      const ConstantInt* c = 
    //             dynamic_cast<const CONST_INT&> (e->op ()).get ();
	      
    //           return 
    // 		msat_make_number(env, 
    // 				 c->getValue().toString(10, true).c_str());
    // 	    }
    //       if (isOpX<INT>(e)){
    //         return msat_make_number(env, 
    // 				    boost::lexical_cast<std::string>(e.get()).c_str());
    //       }
    // 	  if (isOpX<MPQ>(e))
    // 	    {
    // 	      const MPQ& op = dynamic_cast<const MPQ&>(e->op ());
    //           return msat_make_number 
    // 		(env, boost::lexical_cast<std::string> (op.get()).c_str ());
    // 	    }
    // 	  if (isOpX<MPZ>(e))
    // 	    {
    // 	      const MPZ& op = dynamic_cast<const MPZ&>(e->op ());
    //           return msat_make_number 
    // 		(env, boost::lexical_cast<std::string> (op.get()).c_str ());
    // 	    }
	  
          
    //       // -- this checks if the variable is boolean
    //       // -- added for boolean abstraction (AllSat)
    //       if (isOpX<STRING>(e)){
    //         std::string svar = boost::lexical_cast<std::string>(e.get());
            
    //         if (svar[0] == 'b'){
    //           if (exprMap.find(e) != exprMap.end()){
    //             return exprMap[e];
    //           }
    //           msat_decl mdecl = msat_declare_variable(env, svar.c_str(), MSAT_BOOL);
    //           assert(!MSAT_ERROR_DECL(mdecl));
    //           msat_term res = msat_make_variable (env, mdecl);
    //           assert(!MSAT_ERROR_TERM(res));
    //           exprMap[e] = res;
    //           msatMap[msat_term_id(res)] = e;
    //           return res;
    //         }
    //         assert(false && "Unhandled string in expr2msat");
    //       }

    //       if (isOpX<VALUE>(e)){
    //         std::string svar = "x" + 
    // 	      boost::lexical_cast<std::string,void*>(e.get());
            
    //         const Value* c = 
    // 	      dynamic_cast<const VALUE&> (e->op ()).get ();

    //         assert (c != NULL && "can't be null");

    //         const IntegerType* it =
    // 	      dynamic_cast<const IntegerType*>(c->getType());
            
    //         //assert(it != NULL && "can't be null2");
          
    //         msat_decl mdecl;
    //         if (it != NULL){
    //           if (it->getBitWidth() == 1){
    //             mdecl = msat_declare_variable(env, svar.c_str(), MSAT_BOOL);
    //           }
    //           else  
    //             mdecl = msat_declare_variable(env, svar.c_str(), numeralSort);
    //         }else{
    // 	      mdecl = msat_declare_variable(env, svar.c_str(), numeralSort);
    //         }
    //         msat_term res = msat_make_variable (env, mdecl);
    //         assert(!MSAT_ERROR_TERM(res));
    // 	    // no need to update reverse lookup map
    //         exprMap[e] = res;
    // 	    msatMap[msat_term_id(res)] = e;
    //         return res;
    //       }

    // 	  // XXX Not sure if we can get BBs in interpolants
    //       if (isOpX<BB>(e)){
    //         std::string svar = "BB" + boost::lexical_cast<std::string,void*>(e.get());
            
    //         msat_decl mdecl = 
    //           msat_declare_variable(env, svar.c_str(), MSAT_BOOL);
    //         assert(!MSAT_ERROR_DECL(mdecl));
    //         msat_term res = msat_make_variable (env, mdecl);
    //         assert(!MSAT_ERROR_TERM(res));
    //         exprMap[e] = res;
    // 	    msatMap[msat_term_id(res)] = e;
    //         return res;
    //       }

    // 	  if (isOpX<NODE> (e))
    // 	    {
    // 	      std::string svar = "N" + 
    // 		boost::lexical_cast<std::string,void*>(e.get());
    // 	      msat_decl mdecl = 
    // 		msat_declare_variable(env, svar.c_str(), MSAT_BOOL);
    // 	      assert(!MSAT_ERROR_DECL(mdecl));
    // 	      msat_term res = msat_make_variable (env, mdecl);
    // 	      assert(!MSAT_ERROR_TERM(res));
    // 	      // no need to update reverse lookup map
    // 	      exprMap[e] = res;
    //           msatMap[msat_term_id(res)] = e;	      
    //           return res;
    // 	    }

          
    //       //errs() << "\n" <<e << "---->";
    // 	  assert (0 && "Unknown 0-arity expression");
    // 	}

    //   msat_term res;
    //   // -- clear up result (for asserts later)
    //   MSAT_MAKE_ERROR_TERM(res);
    //   if (arity == 1)
    // 	{
    // 	  //then it's a NEG or UN_MINUS or ABS
    // 	  //XXX not sure how to handle ABS in mathsat
    // 	  if (isOpX<UN_MINUS>(e))
    // 	    res = msat_make_negate(env, expr2msat (e->left()));
    // 	  if (isOpX<NEG>(e))
    // 	    res = msat_make_not(env, expr2msat (e->left()));
    // 	  assert (!(MSAT_ERROR_TERM(res)) && "Unknown unary expression");
    // 	}
    //   else if (arity == 2)
    // 	{
    // 	  if (isOpX<VARIANT>(e))
    // 	    {
    // 	      int num = variant::variantNum(e);
    // 	      Expr var = variant::mainVariant(e);
    //           std::string svar;
    // 	      svar = "v" + 
    // 	        boost::lexical_cast<std::string,void*>(var.get()) +
    // 		"_" + boost::lexical_cast<std::string>(num);

                        
    // 	      msat_decl mdecl;
    // 	      if (isOpX<BB> (var))
    // 		{mdecl = msat_declare_variable(env, svar.c_str(), MSAT_BOOL);
    // 		  assert(!MSAT_ERROR_DECL(mdecl));
    // 		}
    // 	      else{
    //             Expr exp = var;
                
    //             if (isOp<VARIANT>(var))
    //               exp = variant::mainVariant(var);

    //             const Value* c = 
    // 		  dynamic_cast<const VALUE&> (exp->op ()).get ();

    //             assert (c != NULL && "can't be null");

    //             const IntegerType* it =
    // 		  dynamic_cast<const IntegerType*>(c->getType());
            
    //             //assert(it != NULL && "can't be null2");
    //             if (it == NULL)
    //               mdecl = msat_declare_variable(env, svar.c_str(), numeralSort);
    //             else{
    // 		  if (it->getBitWidth() == 1){
    // 		    mdecl = msat_declare_variable(env, svar.c_str(), MSAT_BOOL);
    // 		    assert(!MSAT_ERROR_DECL(mdecl));
    // 		  }
    // 		  else  
    // 		    mdecl = msat_declare_variable(env, svar.c_str(), numeralSort);
    //               assert(!MSAT_ERROR_DECL(mdecl));
    //             }
    //           }

    // 	      res = msat_make_variable (env, mdecl);
    // 	      // -- update reverse lookup map
    // 	      msatMap [msat_term_id (res)] = e;
    // 	    }
    // 	  else 
    // 	    {
    // 	      msat_term t1 = expr2msat(e->left());
    // 	      msat_term t2 = expr2msat(e->right());

    // 	      /** BoolOp */
    // 	      if (isOpX<AND>(e)) {
    //             res = msat_make_and(env, t1, t2);
    //           }
    // 	      else if (isOpX<IMPL>(e))
    // 		res = msat_make_implies(env,t1, t2);
    // 	      else if (isOpX<OR>(e))
    // 		res = msat_make_or(env, t1, t2);
    // 	      else if (isOpX<IFF>(e))
    // 		res = msat_make_iff(env, t1, t2);
    // 	      else if (isOpX<XOR>(e))
    // 		res = msat_make_xor(env, t1, t2);

    // 	      /** NumericOp */
    // 	      else if (isOpX<PLUS>(e))
    // 		res = msat_make_plus(env, t1, t2);
    // 	      else if (isOpX<MINUS>(e))
    // 		res = msat_make_minus(env, t1,t2);
    // 	      else if (isOpX<MULT>(e))
    // 		res = msat_make_times(env, t1, t2);
    // 	      else if (isOpX<DIV>(e))
    // 		res = msat_make_divide(env, t1, t2);

    // 	      /** Comparisson Op */
    // 	      else if (isOpX<EQ>(e))
    // 		res = msat_make_equal(env, t1, t2);
    // 	      else if (isOpX<NEQ>(e))
    // 		res = msat_make_not(env, msat_make_equal(env, t1, t2));
    // 	      else if (isOpX<LEQ>(e))
    // 		res =  msat_make_leq(env, t1, t2);
    // 	      else if (isOpX<GEQ>(e))
    // 		res = msat_make_geq(env, t1, t2);
    // 	      else if (isOpX<LT>(e))
    // 		res = msat_make_lt(env, t1, t2);
    // 	      else if (isOpX<GT>(e))
    // 		res = msat_make_gt(env, t1, t2);
    // 	      assert(!MSAT_ERROR_TERM(res));
    // 	    }
    // 	}
    //   else
    // 	{
    // 	  // XXX it must be AND or OR nary expression
    // 	  // XXX because we can't get nary + or - from llvm
    // 	  // XXX and msat doesn't return nary terms??

    // 	  std::vector<msat_term> args;
      
    // 	  for (ENode::args_iterator it = e->args_begin(), end = e->args_end();
    // 	       it != end; ++it)
    // 	    args.push_back (expr2msat (*it));
          
    //       if (isOp<ITE>(e)){
    //         res = msat_make_ite(env,args[0],args[1],args[2]);
    //       }
    //       else if (isOp<AND>(e))
    // 	    {
	      
    // 	      res = args [0];
    // 	      for (size_t i = 1; i < args.size(); ++i)
    // 		res = msat_make_and(env, res, args[i]);
    // 	    }
    // 	  else if (isOp<OR>(e))
    // 	    {
    // 	      res = args[0];
    // 	      for (size_t i=1; i < args.size(); ++i)
    // 		res = msat_make_or(env, res, args[i]);
    // 	    }
    //       else if (isOp<XOR>(e))
    // 	    {
    // 	      res = args[0];
    // 	      for (size_t i=1; i < args.size(); ++i)
    // 		res = msat_make_xor(env, res, args[i]);
    // 	    } 
 
    // 	}

    //   assert (!MSAT_ERROR_TERM(res)); // one of the above conditions held
    //   exprMap [e] = res;
      
    //   return res;
    // }

    

    // Expr msat2exprold(msat_term m)
    // {
    //   if (msat_term_is_true(m)) return mk<TRUE>(efac);
    
    //   if (msat_term_is_false(m)) return mk<FALSE>(efac);
    
    //   if (msat_term_is_number(m))
    // 	{
    //       // XXX this commented code can be used to produce 
    //       // a ConstantInt* Expr for an integer value.
    // 	  //std::string snum = msat_term_repr (m);
    // 	  //const ConstantInt* c = ConstantInt::get(Type::getInt32Ty(getGlobalContext()),boost::lexical_cast<int>(snum),true); 
    //       //return mkTerm(c, efac);



    // 	  std::string snum = msat_term_repr (m);
	  
    // 	  if (useGmp)
    // 	    {
    // 	      if (numeralSort == MSAT_REAL)
    // 		{
    // 		  const mpq_class mpq = mpq_class(snum);
    // 		  Expr res = mkTerm (mpq, efac);
    // 		  if (!isOpX<MPQ>(res))
    // 		    errs() << "type " << typeid (res->op ()).name () << "\n"
    // 			      << "MPQ: " << typeid (MPQ).name () << "\n"
    // 			      << "snum: " << snum << "\n";
    // 		  assert (isOpX<MPQ>(res));
    // 		  return res;
    // 		}
    // 	      else
    // 		{
    // 		  const mpz_class mpz(snum);
    // 		  Expr res = mkTerm (mpz, efac);
    // 		  assert (isOpX<MPZ>(res));
    // 		  return res;
    // 		}
    // 	    }
    // 	  else
    // 	    return mkTerm(boost::lexical_cast<int>(snum), efac);
    // 	}
      

    //   if (msat_term_is_not (m))
    // 	return mk<NEG> (msat2expr (msat_term_get_arg (m, 0)));

    //   if (msat_term_is_negate (m))
    // 	return mk<UN_MINUS> (msat2expr (msat_term_get_arg (m, 0)));

    //   msat_map_type::iterator it = msatMap.find (msat_term_id (m));
    //   if (it != msatMap.end ()) return it->second;

    //   /** A variable introduced by MSAT */
    //   if (msat_term_is_variable (m))
    // 	{
    // 	  errs() << msat_term_repr (m) << "\n\n";
    // 	  assert (0 && "Don't expect MSAT to introduce variables!");
    // 	}
      

    //   Expr e;
      
    //   std::vector<Expr> args;

    //   for (size_t i = 0; i < (size_t)msat_term_arity(m); i++){
    // 	args.push_back (msat2expr (msat_term_get_arg(m, i)));
    //   }

    //   if (msat_term_is_and (m))
    // 	e = mknary<AND> (args.begin(), args.end());
    //   else if (msat_term_is_or (m))
    // 	e =  mknary<OR> (args.begin(), args.end());
    //   else if (msat_term_is_xor (m))
    // 	e = mknary<XOR> (args.begin(), args.end());
    //   else if (msat_term_is_iff (m))
    // 	e =  mknary<IFF> (args.begin(), args.end());
    //   else if (msat_term_is_implies (m))
    // 	e =  mknary<IMPL> (args.begin(), args.end());
    //   else if (msat_term_is_equal (m))
    // 	e =  mknary<EQ> (args.begin(), args.end());
    //   else if (msat_term_is_lt (m))
    // 	e =  mknary<LT> (args.begin(), args.end());
    //   else if (msat_term_is_gt (m))
    // 	e =  mknary<GT> (args.begin(), args.end());
    //   else if (msat_term_is_leq (m))
    // 	e =  mknary<LEQ> (args.begin(), args.end());
    //   else if (msat_term_is_geq (m))
    // 	e =  mknary<GEQ> (args.begin(), args.end());
    //   else if (msat_term_is_plus (m))
    // 	e =  mknary<PLUS> (args.begin(), args.end());
    //   else if (msat_term_is_minus (m))
    // 	e =  mknary<MINUS> (args.begin(), args.end());
    //   else if (msat_term_is_times (m)) 
    // 	e =  mknary<MULT> (args.begin(), args.end());
    //   else if (msat_term_is_bv_sdiv (m) || msat_term_is_bv_udiv (m))
    // 	e = mknary<DIV> (args.begin(), args.end());
    //   else if (msat_term_is_bv_urem (m) || msat_term_is_bv_srem (m))
    // 	e = mknary<MOD> (args.begin(), args.end());
    //   else
    // 	assert (0 && "Unknown MSAT expression");
    //   msatMap [msat_term_id(m)] = e;

    //   return e;
    // }


    // -- passed as last parameter to allsatCallback. Passes
    // -- information about the msat instance
    struct cbWrapper
    {
      ExprVector res;
      this_type *ms;
    };

    // -- used by allSAT to collect models
    static int allsatCallback (msat_term * model, int size, void* result)  
    {
      errs () << '.';
      errs ().flush ();
      
      cbWrapper* wrp = (cbWrapper*)result;
      
      if (size == 0) 
	{ wrp->res.push_back (mk<TRUE>(wrp->ms->efac)); return 1; }
      
      if (size == 1) 
	{ wrp->res.push_back (wrp->ms->msat2expr (model [0])); return 1; }
      
      
      ExprVector clause;
      for (int i=0; i<size; i++)
	clause.push_back (wrp->ms->msat2expr (model [i]));
      
      wrp->res.push_back 
	(mknary<AND> (clause.begin (), clause.end ()));
      
      return 1;
    }

  };


}
#endif
