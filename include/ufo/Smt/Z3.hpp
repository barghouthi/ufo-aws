#ifndef __UFO_Z3_HPP_
#define __UFO_Z3_HPP_

#include "ufo/ufo.hpp"
#include "z3.h"

#include "boost/range.hpp"

using namespace llvm;
using namespace expr;
using namespace expr::op;

namespace ufo
{
  typedef std::map<std::string,std::string> StringMap;
  typedef std::pair<std::string, std::string> StringPair;
}

namespace ufoz3
{
  void error_handler (Z3_error_code);
  
  /** parameterized by type T of expressions and a converter EC
      between mathsat expressions and T */
  template <typename T, typename EC> 
  class Z3 
  {
  private:
    typedef Z3<T,EC> this_type; 

    Z3_context ctx;
    Z3_model model;
    ExprFactory &efac;

    // -- map from Expr to Z3_ast
    //typedef std::map<Expr, Z3_ast> expr_map_type;
    typedef boost::unordered_map<Expr,Z3_ast> expr_map_type;
    typedef std::vector<expr_map_type> expr_maps_type;
    expr_maps_type exprMaps;    

    // -- map from Z3_ast id to Expr
    typedef std::map<unsigned, Expr> z3_map_type;
    typedef std::vector<z3_map_type> z3_maps_type;
    z3_maps_type z3Maps;


    typedef ExprVector::const_iterator expr_iterator;

    /** configuration options */
    StringMap options;

    bool doModel;
        
  private:
    /** disable default constructor */
    Z3 (const Z3 &other) : efac(other.efac) { assert (0);  }
    
  public:
    Z3 (ExprFactory& e, const StringMap &op = StringMap (), bool dm = false): 
      ctx (NULL), model (NULL), efac (e), doModel (dm)
    { 
      strong_context_simplifier (false);
      elim_quantifiers (false);
      elim_and (true);
      setModel (doModel);
      setModelCompletion (false);
      
      // -- override default options
      forall (StringPair p, op)
        options[p.first] = p.second;
      init ();
    }

    ~Z3 () 
    { 
      exprMaps.clear ();
      z3Maps.clear ();
      if (ctx && model) Z3_del_model (ctx, model);
      if (ctx) Z3_del_context (ctx); 
    }
    
    
    ExprFactory &getFactory () { return efac; }
    
    bool hasModel () const { return doModel && model; }
    
    
    void strong_context_simplifier (bool v) 
    { 
      options ["STRONG_CONTEXT_SIMPLIFIER"] = v ? "true" : "false";
    }
    void elim_quantifiers (bool v) 
    { 
      options ["ELIM_QUANTIFIERS"] = v ? "true" : "false";
    }
    void elim_and (bool v) 
    { 
      options ["ELIM_AND"] = v ? "true" : "false";
    }
    void setModel (bool v) 
    { 
      options ["MODEL"] = v ? "true" : "false";
    }
    void setModelCompletion (bool v) 
    { 
      if (v) setModel (true);
      options ["MODEL_COMPLETION"] = v ? "true" : "false";
    }
    

  private: 
    /** 
     * Make a configuration object based on current set of options
     * 
     * \return a configuration object. Must be deleted by the caller.
     */
    Z3_config mkConfig ()
    {
      Z3_config cfg = Z3_mk_config ();
      
      typedef std::pair<std::string, std::string> KV;
      foreach (KV kv, options) 
	{
	  Z3_set_param_value (cfg, kv.first.c_str (), kv.second.c_str ());      
	  if (kv.first == "MODEL" && kv.second == "true") doModel = true;
	}
      return cfg;
    }
    

    void init ()
    {
      exprMaps.clear ();
      z3Maps.clear ();
      if (model) 
	{
	  Z3_del_model (ctx, model);
	  model = NULL;
	}
      
      if (ctx) 
	{
	  Z3_del_context (ctx);
	  ctx = NULL;
	}
      

      Z3_config  cfg = mkConfig ();
	  
      ctx = Z3_mk_context (cfg);
      Z3_set_error_handler (ctx, error_handler);
      
      Z3_del_config (cfg);

      exprMaps.push_back (expr_map_type ());
      z3Maps.push_back (z3_map_type ());
    }

  public:

    void startLogging(std::string fname)
    {
      Z3_open_log (ctx, fname.c_str());
      Z3_trace_to_file (ctx, (fname + ".trace").c_str());
    }

    void stopLogging()
    {
      Z3_close_log (ctx);
      Z3_trace_off (ctx);
    }
    
    bool push () 
    { 
      Z3_push (ctx); 
      exprMaps.push_back (exprMaps.back ());
      z3Maps.push_back (z3Maps.back ());
      return true; 
    }

    bool pop () 
    { 
      exprMaps.pop_back ();
      z3Maps.pop_back ();
      Z3_pop (ctx, 1);
      return true; 
    }

    Expr simplify (Expr e)
    {
      return z32expr (Z3_simplify (ctx, expr2z3 (e)));
    }


    /** Assert an expression in the current context
     * 
     * \param[in] e an expression to be asserted
     * \returns true
     */
    bool assertExpr (Expr e)
    {
      Z3_assert_cnstr (ctx, expr2z3 (e));
      return true;
    }

    /** 
	Returns the value of e in the model. NONDET is returned if the
	value cannot be determined
	
	\param[in] e an expression whose value is being thought
	\return value of e or NONDET if e is not constraint in the
	current model
    */
    Expr getModelValue (Expr e)
    {
      Z3_ast v;
      if (model && Z3_eval (ctx, model, expr2z3(e), &v))
	return z32expr (v);

      return mk<NONDET> (efac);
    }


    /** Used for debugging */
    void debugPrintModel ()
    {
      if (!model) return;
      
      unsigned num_constants;
      unsigned i;
      
      errs () << "PRINT MODEL\n";
      errs ().flush ();
      num_constants = Z3_get_model_num_constants(ctx, model);
      for (i = 0; i < num_constants; i++) 
	{
	  Z3_func_decl cnst = Z3_get_model_constant(ctx, model, i);
	  Z3_symbol name = Z3_get_decl_name(ctx, cnst);
	  
	  if (Z3_get_symbol_kind (ctx, name) == Z3_INT_SYMBOL)
	    errs () << Z3_get_symbol_int (ctx, name);
	  else
	    errs () << Z3_get_symbol_string (ctx, name);
	  
	  Z3_ast a = Z3_mk_app (ctx, cnst, 0, 0);
	  Z3_ast v = a;
	  Z3_eval (ctx, model, a, &v);
	  if (v) errs () << " = " << Z3_ast_to_string (ctx, v);
	  errs () << "\n";
	}

      errs () << "MODEL END\n";
      errs ().flush ();
    }


    /** Checks whether current context is SAT
     * 
     * \return true if SAT, false if UNSAT, indeterminate otherwise
     */
    boost::tribool solve ()
    {
      // --  delete old model if needed 
      if (model) 
	{
	  Z3_del_model (ctx, model);
	  model = NULL;
	}
      
      Z3_lbool res = 
	doModel ? Z3_check_and_get_model (ctx, &model) : Z3_check (ctx);

      switch (res)
	{
	case Z3_L_FALSE: return false;
	case Z3_L_UNDEF: return boost::indeterminate;
	case Z3_L_TRUE: return true;
	}
      assert (0 && "UNREACHABLE");
    }
    
    /** Checks that the current context is SAT with assumptions
     */
    template <typename Range, typename OutputIterator>
    boost::tribool solveAssuming (const Range &assumptions, OutputIterator out)
    {
      Z3_ast* z3assume = new Z3_ast [boost::size (assumptions)];
      size_t count = 0;
      foreach (Expr a, assumptions) z3assume [count++] = expr2z3 (a);
      
      // -- variables for the output 
      unsigned core_size;
      Z3_ast* core = new Z3_ast [boost::size (assumptions)];

      Z3_lbool res = Z3_check_assumptions (ctx, 
					   assumptions.size (),
					   z3assume, 
					   doModel ? &model : NULL, 
					   NULL,
					   &core_size, core);
      if (res == Z3_L_FALSE)
	for (unsigned i = 0; i < core_size; ++i)
	  *(out++) = z32expr (core [i]);
      
      delete [] core;
      delete [] z3assume;
      
      return z3lbool (res);
    }

    Expr allSAT (Expr fmla, const ExprVector &important)
    {
      assert (doModel);
      Expr res = mk<FALSE> (efac);

      assertExpr (fmla);

      errs () << "in all sat: ";

      while (solve ())
	{
	  assert (model);
	  
	  Expr cube = mk<TRUE> (efac);
	  forall (Expr t, important)
	    {
	      Expr v = getModelValue (t);
	      if (isOpX<TRUE> (v)) cube = boolop::land (cube, t);
	      else if (isOpX<FALSE> (v))
		cube = boolop::land (cube, boolop::lneg (t));
	    }

	  res = boolop::lor (res, cube);
	  assertExpr (boolop::lneg (cube));
	  errs () << ".";
	  errs ().flush ();
	  //errs () << "\tcube: " << *(boolop::pp(cube)) << "\n";
	}
      errs () << "\n";
      return res;
    }
    
    
  private:
    /** Helper method to convert between Z3_lbool and boost::tribool */
    tribool z3lbool (Z3_lbool v)
    {
      switch (v)
	{
	case Z3_L_FALSE: return false;
	case Z3_L_TRUE: return true;
	default: return boost::indeterminate;
	}
    }
    
  public:    
    /** universally eliminate given variables in an expression */
    Expr forallElim (Expr e, const ExprSet &vars)
    {
      //startLogging ("/tmp/z3.log");
      
      unsigned num_bound = vars.size ();
      Z3_app* bound = new Z3_app [num_bound];
      
      size_t count = 0;
      foreach (Expr var, vars)
	{
	  Z3_ast ast = expr2z3 (var);
	  assert (Z3_get_ast_kind (ctx, ast) == Z3_APP_AST);
	  bound [count++] = Z3_to_app (ctx, ast);
	}

      Z3_ast forall = Z3_mk_forall_const (ctx, 
					  0, /* weight */
					  num_bound, 
					  bound, 
					  0, NULL, expr2z3 (e));
      Expr res = z32expr (Z3_simplify (ctx, forall));

      delete [] bound;
      return res;
    }
    
    /** 
     * Returns a string representing the formula as an SMT-LIB2 benchmark
     * 
     * \param formula an input formula
     * \returns a string in SMT-LIB2 format
     */
    std::string toSmtlibBenchStr (Expr formula)
    {
      Z3_set_ast_print_mode (ctx, Z3_PRINT_SMTLIB2_COMPLIANT);

      Z3_ast z3f = expr2z3 (formula);
      std::string name = "ufo_" + 
	boost::lexical_cast<std::string,void*> (formula.get ());
      
      Z3_string bench = Z3_benchmark_to_smtlib_string
	(ctx, name.c_str (), "", "", "", 0, NULL, z3f);
      return std::string (bench);
    }
    
    /** 
     * Converts the formula into SMT-LIB2 format. Note that the output
     * is the formula itself without variable declarations.
     * 
     * \param f an input expression
     * \returns SMT-LIB2 string
     */
    std::string toSmtLib2Str (Expr f)
    {
      Z3_set_ast_print_mode (ctx, Z3_PRINT_SMTLIB2_COMPLIANT);

      Z3_ast z3f = expr2z3 (f);
      Z3_string res = Z3_ast_to_string (ctx, z3f);
      return std::string (res);
    }
    
    /** Returns a string of declarations of all the variables 
	in f in SMT-LIB2 format
	
	\param f input expression
	\returns SMT-LIB2 string of declarations
     */
    std::string toSmtLib2DeclStr (Expr f)
    {
      /** constants */
      if (isOpX<TRUE> (f) || isOpX<FALSE> (f) || 
	  isOpX<MPQ> (f) || isOpX<INT> (f)) return "";
     
      Z3_set_ast_print_mode (ctx, Z3_PRINT_SMTLIB2_COMPLIANT); 
      // -- convert to z3
      Z3_ast z3f = expr2z3 (f);

      std::set<Z3_func_decl> seen;
      return toSmtLib2DeclStr (z3f, seen);
    }

    Expr fromSmtLib2Str (std::string s)
    {
      Z3_ast x;
      x = Z3_parse_smtlib2_string (ctx, s.c_str (), 
				   0, NULL, NULL, 0, NULL, NULL);
      return z32expr (x);
    }
    
    
  private:

    std::string toSmtLib2DeclStr (Z3_ast f, std::set<Z3_func_decl> &seen)
    {
      Z3_ast_kind kind = Z3_get_ast_kind (ctx, f);
      if (kind != Z3_APP_AST) return "";

      Z3_app app = Z3_to_app (ctx, f);
      Z3_func_decl  fdecl = Z3_get_app_decl (ctx, app);
      Z3_decl_kind dkind = Z3_get_decl_kind (ctx, fdecl);
      
      if (seen.count (fdecl) > 0) return "";

      std::string res = "";
      
      if (dkind == Z3_OP_UNINTERPRETED)
	{
	  res = funcDeclToStr (fdecl);
	  res += "\n";
	  seen.insert (fdecl);
	}
      
      for (unsigned i = 0; i < Z3_get_app_num_args (ctx, app); ++i)
	res += toSmtLib2DeclStr (Z3_get_app_arg (ctx, app, i), seen);

      return res;
    }
    

    /** Z3 v3.2 has a buggy implementation of z3_func_decl_to_string()
     * This is a substitute for it
     **/
    std::string funcDeclToStr (Z3_func_decl fdecl)
    {
      assert (Z3_get_decl_num_parameters (ctx, fdecl) == 0 && 
	      "Only support constants for now");
      
      std::string res;
      res = "(declare-const ";
      Z3_symbol name = Z3_get_decl_name (ctx, fdecl);
      assert (Z3_get_symbol_kind (ctx, name) == Z3_STRING_SYMBOL);
      res += Z3_get_symbol_string (ctx, name);
      res += " ";

      Z3_sort sort = Z3_get_range (ctx, fdecl);
      switch (Z3_get_sort_kind (ctx, sort))
	{
	case Z3_BOOL_SORT: 
	  res += "Bool";
	  break;
	case Z3_INT_SORT:
	  res += "Int";
	  break;
	case Z3_REAL_SORT:
	  res += "Real";
	  break;
	default:
	  assert (0 && "Unsupported SORT");
	}
      
      res += ")";
      return res;
    }
    

    Z3_ast expr2z3 (Expr e)
    {
      Stats::resume ("z3.from_expr");      
      Z3_ast res = EC::marshal (ctx, e, exprMaps.back (), z3Maps.back ());
      Stats::stop ("z3.from_expr");
      return res;
    }
    Expr z32expr (Z3_ast z)
    {
      Stats::resume ("z3.to_expr");
      Expr res = EC::unmarshal (ctx, z, efac, z3Maps.back ());
      Stats::stop ("z3.to_expr");
      return res;
    }
  };  
}


#endif
