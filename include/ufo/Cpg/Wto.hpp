#ifndef _WTO__H_
#define _WTO__H_

#include <map>
#include <typeinfo>
#include <string>
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Pass.h"
#include "llvm/Function.h"
#include "llvm/Instructions.h"
#include "llvm/Constants.h"
#include "llvm/Analysis/Dominators.h"

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>
#define forall BOOST_FOREACH
#include "ufo/Cpg/Lbe.hpp"

namespace llvm
{
  /** This is internal to the computation. Use class WTO below. */
  /** An interval class used for computing weak topological orders (WTO) */
  class WTOInt
  {
    /** The head of the interval, this is either interval[0] or
	interval[1] based on existence of a bracket */
    CutPointPtr head;

    /** list of cutpoints in the interval (including the head) for
	left and right brackets we use special cutpoints with ids -1 and
	-2, respectively */
    std::vector<CutPointPtr> interval;
    
    /** successors of this interval */
    std::vector<WTOInt*> succ;
    
    /** predecessors of this interval */
    std::vector<WTOInt*> preds;
  public:

    typedef std::vector<CutPointPtr>::iterator succ_iterator;

    WTOInt (CutPointPtr cp) : head(cp) { interval.push_back(cp); }

    /** copy constructor */
    // XXX What happens to preds and succs?
    WTOInt(const WTOInt &i) : head(i.head), interval(i.interval) {}
    
    // WTOInt &operator= (const WTOInt &other)
    // {
    //   WTOInt tmp(other);
    //   std::swap (other.interval, interval);
    //   head = other.head;
    //   // XXX What about preds and succs?
    // }
    

    CutPointPtr getHead () const { return head; }
    
    void addSucc (WTOInt *i) { succ.push_back(i); }
    void addPred (WTOInt *i) { preds.push_back(i); }

    std::vector<CutPointPtr> &getInterval () { return interval; }
    const std::vector<WTOInt*> &getPreds () const { return preds; }
    const std::vector<WTOInt*> &getSuccs () const { return succ; }
    
    

    /** append a cutpoint to the interval */
    void append(CutPointPtr cp){ interval.push_back(cp);  }
    
    /**  appends interval t to the current interval */
    void mergeIn (WTOInt& t)
    {
      bool addB = false;
      CutPointPtr back;
      
      // --  if the interval ends with a closing bracket, remove and remember
      if (!interval.empty())
	{
	  back = interval.back ();
	  if (back->getID() < 0)
	    {
	      interval.pop_back();
	      addB = true;
	    }
	}
      
      forall (CutPointPtr cp, t.interval) append (cp);

      // -- restore closing bracket
      if (addB) append(back);
    }

    // -- appends interval t to the current interval
    void merge(WTOInt& t) 
    {
      forall (CutPointPtr cp, t.interval) append (cp);
    }

  };
  
  /** Weak Topological Order */
  class WTO
  {
    CutPointPtr cp;
    
    std::vector<WTO*> children;
    WTO *parent;
    
    bool comp;

  public:
    WTO(CutPointPtr c, WTO *p): cp(c), parent(p), comp(false) {}
    
    void addChild(WTO *c) { children.push_back(c); }
    bool getComp () const { return comp; }
    void setComp (bool v) { comp = v; }

    CutPointPtr getCp () const { return cp; }
    std::vector<WTO*> &getChildren () { return children; }
    

    
    WTO* getParent () const { return parent; }
    void setParent (WTO &v) { parent = &v; }
    
  };

  struct WTOPass : public ModulePass
  {
    Function* F;
    static char ID;
    
    LBE* lbe;
    // -- pair of intervals
    typedef std::pair<WTOInt*, WTOInt*> ip;
    
    /** left and right brackets in an interval*/
    CutPointPtr leftB;
    CutPointPtr rightB;
    
    // -- map from component id to depth
    std::map<int,int> comp2depth;
    
    // -- map from CutPoint IDs to components
    std::map<int,int> cp2comp;
    
    std::map<CutPointPtr, WTO*> cp2wto;

    // -- map from component ids to components
    std::map<int,std::vector<CutPointPtr> > id2comp;

    WTOInt* wtoOrder;
    
    /**  this computes an intervals graph for the function F. If i is
	 NULL, then it computes the interval graph of F, otherwise, it
	 computes the interval graph over i. That is, it computes the
	 limit-flow graph, returning the root interval */
    WTOInt* computeIntervals(WTOInt* i,  Function &F, int &size);
    
    WTO* computeWTO (WTOInt*);

    CutPointPtr wtoNextSame(const CutPoint &cp);
    CutPointPtr nextWtoHead(const CutPoint &cp);
    bool isWtoHead(const CutPoint &cp);
    
    bool isInCompOrSub(const  CutPoint &ref, const CutPoint &l);
    
    WTOInt* createInitialGraph(Function& F);

    /** main function for computing a weak topo order */
    void weakTopologicalOrder(Function* F);
 
    bool runOnModule (Module &M);

    void addSelfLoops(Function& F, WTOInt* g);

    void deleteGraph(WTOInt* g);

    WTOPass () : ModulePass (ID) 
          { errs () << " Creating WTOPass\n";}	
    
    virtual void getAnalysisUsage (AnalysisUsage &AU) const 
    {
      AU.addRequired<Topo>();
      AU.addRequired<LBE>();
      AU.addRequired<UnifyFunctionExitNodes>();
      AU.setPreservesAll ();
    }
  };

}



#endif
