#ifndef __ARG__COND__HPP_
#define __ARG__COND__HPP_

/** 
 * Computes an ARG condition: a formula whose satisfying assignments
 * correspond to executions through an ARG. Formally, the result of
 * the computation is an EdgeMap emap of the ARG such that for any
 * path P = p_1, ..., p_k through the ARG, every satisfying assignment to
 * the formula AND_{i\in[1,.k)} emap(p_i,p_{i+1}) corresponds to an
 * execution through the path P
 */


/* 
   pseudocode:

   Input: 
      Graph G -- the ARG
      Range topo -- topological order of vertices (G)
      VertexMap : vertices(G) -> Node*  map from verticies to ARG nodes
      + factories, solvers, etc.
   Output:
      envMap: VertexMap : verticies (G) -> Environment
        a map from vertecies to environments for them
        if phi is a formula over program varialbes, and n a vertex, then
          phi' = eval (phi, envMap (n)) is phi relative to Node n

      argCond : EdgeMap : edges (G) -> Expr
        a map from edges of G to edge conditions.

   PROC argCond (G, topo, nmap)
     // -- the computation is a side-effect of computing environments
     // -- for each node
     foreach v in topo do  computeNodeEnv (v)
   END

   PROC computeNodeEnv (Node v)

     Map newEnv
     foreach p in parents_of (v) do
       // -- computeEdgeCond returns a pair Expr,Env of an edge
       // -- condition and new environment
       (argCond[(p,v)], newEnv[p]) = computeEdgeCond (envMap[p], p, v)

    // -- join environments
    Environments vEnv = joinEnv (newEnv);
    // -- output the new environment
    envMap [v] = vEnv;
    
    // -- add resolution equialities to map between the environment of
    // -- p and the joined environment
    foreach p in parents_of (v) do
       argCond [p,v] = argCond[p,v] AND resolver (p, newEnv[p], v, vEnv)

    // -- DONE
   END
 */


#include "ufo/ufo.hpp"
#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"
#include "ufo/Expr.hpp"

#include <boost/range.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/property_map/property_map.hpp>

#include "ufo/ufo_graph.hpp"

namespace ufo
{
  namespace argcond
  {
    inline Expr mkEdgVar (Expr src, Expr dst)
    {
      return bind::boolVar (mk<TUPLE> (src, dst));
    }    
    
    inline Expr mkEdgVar (ExprFactory &efac, NodePair edg)
    {
      Expr src = mkTerm (edg.first, efac);
      Expr dst = mkTerm (edg.second, efac);
      return mkEdgVar (src, dst);
    }
  }
  
  using namespace argcond;
  
  template<typename Graph>
  class ArgCond
  {
  private:
    typedef boost::graph_traits<ARG> Traits;
    typedef Traits::vertex_descriptor Vertex;
    typedef Traits::edge_descriptor Edge;

    /** Expression factory */
    ExprFactory &efac;
    /** Large Block Encoding : Cutpoint Graph */
    LBE &lbe;
    /** Dominator Tree */
    DominatorTree &DT;

    bool withAssume;
  public:
    /**
     * Creates an ArgCond object
     * 
     * \param[in] f an Expression Factory
     * \param[in] l Cutpoint graph
     * \param[in] d Domainator tree
     * \param[in] assume if true, generate formula with assumptions
     */
    ArgCond (ExprFactory &f, LBE &l, DominatorTree &d, bool assume = false) :
      efac (f), lbe (l), DT (d), withAssume (assume) {}
    

    /** Computes the ARG condition.
     * 
     * \tparam Range a boost::range
     * \tparam EnvMap a property map from verticies to environments
     * \tparam EdgeMap a property map from edges to expressions

     * \tparam EdgeBbMap a map from edges to std:map<BasicBlock*,
     * Expr> that records the varialbe each basic block on the edge is
     * represented by
     * 
     * \param[in] g the input ARG. g has a unique root node.
     * \param[in] topo topological order of vertices of g
     * \param[out] envMap output environment map
     * \param[out] argCond output Arg Condition
     * 
     * \return true if there is an interpolant, false if the formula is
     * satisfiable, and indeterminate if SMT-solver failed to decide the
     * formula.
     */
    template <typename Range, typename EnvMap, typename EdgeMap, 
	      typename EdgEnvMap>
    void argCond (const Graph &g, Range &topo, EnvMap &envMap, 
		  EdgeMap &argCond, EdgEnvMap &edgEnvPM)
    {
      foreach (Node *v, topo) computeNodeEnv (v, g, envMap, argCond, edgEnvPM);
    }

    template <typename Range, typename EnvMap, typename EdgeMap>
    void argCond_ (const Graph &g, Range &topo, EnvMap &envMap, 
		  EdgeMap &argCond)
    {
      std::map<Edge, std::map<const BasicBlock*, Expr> > bbMap;
      BOOST_AUTO(bbPM, make_assoc_property_map (bbMap));
      argCond (g, topo, envMap, argCond, bbPM);
    }

    

  private:
    /** 
     * Computes the environment of a given node given on environments
     * of its predecessors. Computes the ARG condition of the incoming
     * edges as a side-effect.
     *
     * \tparam EnvMap a property map from verticies to environments
     * \tparam EdgeMap a property map from edges to expressions
     *
     * \param[in] v the node
     * \param[in] g the input ARG
     * \param[out] envMap output environment map
     * \param[out] argCond output Arg Condition
     */
    template <typename EnvMap, typename EdgeMap, typename EdgEnvMap>
    void computeNodeEnv (Node *v, const Graph &g, 
			 EnvMap &envMap, EdgeMap &argCond, EdgEnvMap &edgEnvPM)
    {
      // -- root node
      if (in_degree (v, g) == 0) 
	{
	  Environment rootEnv (efac);
	  SEdgeCondComp::updateEnvToFirstInst (rootEnv, *v->getLoc ());
	  put (envMap, v, rootEnv);
	  return;
	}

      // -- inner nodes

      // -- propagate environments over incoming edges
      // -- and compute edge conditions for incoming edges
      std::map<Node*,Environment> pEnvs;
      foreach (Edge edg, in_edges (v, g))
	{
	  // -- doing edge (p, v)
	  Node *p = source (edg, g);
	  SEdgePtr sedge = *(p->getLoc ()->findSEdge (*v->getLoc ()));

	  // -- copy  parent's environment
	  pEnvs[p] = get (envMap, p);

	  Expr eCond = SEdgeCondComp (efac, pEnvs [p], 
				      sedge, lbe, 
				      withAssume).compute ().getCond ();
	    // SEdgeCondComp::computeEdgeCond (efac, pEnvs[p],
	    // 					       sedge, lbe);

	  // -- edge condition of a FALSE_EDGE is FALSE
	  if (get (false_edge_prop_t(), g, edg))
	    put (argCond, edg, mk<FALSE> (efac));
	  else
	    {
	      put (argCond, edg, eCond);
	      put (edgEnvPM, edg, pEnvs [p]);
	    }
	  
	}
      
      // -- do not compute the environment of the last node it is
      // -- never used since there is never anything evaluated at the
      // -- last node. 
      if (out_degree (v, g) == 0) return;
      
      // -- merge incomming environments
      Environment vEnv;
      NodeExprMap resolvers;
      joinEnvs (g, v, pEnvs, vEnv, resolvers);
      SEdgeCondComp::updateEnvToFirstInst (vEnv, *v->getLoc ());
      put (envMap, v, vEnv);
      
      // -- update edge conditions with resolvers
      foreach (Edge edg, in_edges (v, g))
	{
	  // -- skip false edges for efficiency
	  if (get (false_edge_prop_t(), g, edg)) continue;
	  Expr resolveE = resolvers.find (source (edg, g))->second;
	  if (isOpX<TRUE> (resolveE)) continue;
	      
	  Expr eCond = get (argCond, edg);
	  // -- store edg condition as (cond, resolvers) if there is
	  // -- an explicit list of resolvers
	  put (argCond, edg, mk<TUPLE> (eCond, resolvers[source (edg, g)]));
	}
    }

    void fillBbToVar (const Environment &env, 
		      std::map<const BasicBlock*, Expr> &bbToVar)
    {
      forall (ExprPair ep, make_pair (env.begin (), env.end ()))
	{
	  if (!isOpX<BB> (ep.first)) continue;
	  const BasicBlock *bb = getTerm<const BasicBlock*> (ep.first);
	  bbToVar [bb] = ep.second;
	}
    }
    
    
    /** 
     * Joins environments at a node.
     *
     * The join operation keeps all the bindings on which all
     * environments agrees, refreshes bindings in which they disagree,
     * and removes bindings which will not be used from this point on
     * (i.e., dead variables).
     *
     *
     * \param[in] g the input ARG
     * \param[in] v the node at which environments are joined
     * \param[in] pEnvs the environments being joined and the nodes
     * they came from
     * \param[out] out the joined environment
     * \param[out] resolves maps each parent node p to an expression r
     * that unifies environment pEnvs[p] with the output environment out
     *
     */
    void joinEnvs (const Graph &g, Node* v,
		   std::map<Node*, Environment> &pEnvs,
		   Environment &out,
		   NodeExprMap &resolvers)
    {
      // -- Computed values of pEnvs map. 
      // XXX Learn to use boost range adapters instead
      std::vector<Environment*> pEnvsValues;
      // XXX not using foreach to get reference into the map
      for (std::map<Node*,Environment>::iterator 
	     it = pEnvs.begin (), end = pEnvs.end (); it != end; ++it)
	pEnvsValues.push_back (&(it->second));
      
      std::vector<Environment*>::const_iterator it = pEnvsValues.begin ();
      std::vector<Environment*>::const_iterator end = pEnvsValues.end ();

      // -- there is at least one element to join
      assert (it != end);

      // -- grab first environment
      out = *(*it);
      ++it;

      // -- merge the rest
      ExprSet refreshed; // -- vars that were refreshed
      out.unify (it, end, refreshed);

      // -- remove bad bindings
      cleanJoin (out, v);
      
      // -- remove bindings from refreshed that were removed by cleanJoin
      ExprSet::iterator refIt = refreshed.begin ();
      while (refIt != refreshed.end ())
	{
	  Expr key = *refIt;
	  if (out.isBound (key)) ++refIt;
	  else refreshed.erase (refIt++);
	}
      
      foreach (Edge edg, in_edges (v, g))
	{
	  Node* p = source (edg, g);
	  if (get (false_edge_prop_t(), g, edg)) continue;
	  
	  ExprVector resolved;
	  Environment &edgeEnv = pEnvs[p];
	  
	  foreach (Expr key, refreshed)
 	    {
	      Expr val = key;
	      if (isOpX<VARIANT> (val)) val = variant::mainVariant (val);
	      if (isOpX<VARIANT> (val)) val = variant::mainVariant (val);
	      
	      if (isOpX<BB> (val))
		{
		  resolved.push_back 
		    (mk<IFF> (edgeEnv.lookup (key), out.lookup (key)));
		  continue;
		}

	      assert (isOpX<VALUE> (val));     
	      const Value *c = getTerm<const Value*> (val);
	      
	      Expr phi;
	      if (isBoolType (c->getType()))
		phi = mk<IFF> (edgeEnv.lookup(key), out.lookup (key));
	      else
		{
		  phi = mk<EQ>(edgeEnv.lookup(key), out.lookup (key));
		  // -- stating x <= y && x >= y  seems to trick mathsat into 
		  // -- not propagating around the equality x=y. 
		  // phi = mk<AND> (mk<LEQ> (edgeEnv.lookup(key), 
		  // 			  out.lookup (key)),
		  // 		 mk<GEQ> (edgeEnv.lookup(key), 
		  // 			  out.lookup (key)));
	    
		}
	      resolved.push_back (phi);
	    }
	  
	  resolvers[p] = mknary<AND> (mk<TRUE> (efac), 
				      resolved.begin (), resolved.end ());
	}
    }

    /** 
     * Removed dead bindings from a joined environment.
     * 
     * This is based on heuristics. Can be re-done using LoopSSA to
     * put the ARG into SSA prior to encoding.
     * 
     * \param[inout] env the environment being cleaned
     * \param[in] the node with respect to which the environment is cleaned
     */
    void cleanJoin (Environment& env, Node *node)
    {
      const BasicBlock* nodeBB = node->getLoc ()->getBB ();
      
      Environment::iterator it = env.begin ();      
      while (it != env.end ())
	{
	  Expr key = it->first;
	      
	  if (isOpX<BB> (key)) { env.erase (it++); continue; }
	  else if (isOpX<VARIANT>(key))
	    {
	      Expr mv = variant::mainVariant (key);
	      assert (isOpX<VALUE>(mv));
	      
	      const Value* v = getTerm<const Value*> (mv);
	      assert(v != NULL);
	      
	      const Instruction* inst = dyn_cast<const Instruction>(v);
	      assert(inst != NULL);
	      
	      assert (isa<PHINode> (inst));
	      // -- only care about PhiNodes that are defined in the node
	      if (inst->getParent () != nodeBB){ 
		env.erase (it++); 
		continue;
	      }
	    }

	  else if (isOpX<VALUE>(key))
	    {
	      const Value* v = getTerm<const Value*> (key);
	      assert(v != NULL);
            
	      const Instruction* inst = dyn_cast<const Instruction>(v);
	      assert(inst != NULL);

	      // -- anything that is defined in the basic block is
	      // -- defined before used, so we don't need it from
	      // -- other environments.
	      if (inst->getParent () == nodeBB) 
		{
		  env.erase (it++);
		  continue;
		}
	      // -- does not dominate the basic block, must be out of scope
	      else if (!DT.dominates (inst->getParent (), nodeBB))
		{
		  env.erase (it++);
		  continue;
		}
	      else
		{
		  // -- keep any value that is defined and used in different
		  // -- blocks except if the only different-block-use of the
		  // -- value is in a PHI assignment of nodeBB. 
		  bool keep = false;
		  
		  for (Value::const_use_iterator ut = inst->use_begin(), 
			 ue = inst->use_end (); ut != ue; ++ut)
		    {
		      const User* u = *ut;
		      const BasicBlock* ubb = 
			cast<Instruction>(u)->getParent ();
		      
		      if (ubb != inst->getParent())
			{
			  // -- found a use in a different block
			  // -- check if it is us
			  if (ubb == nodeBB)
			    {
			      // -- use in the node BB. check if it is a phi use
			      if (isa<PHINode> (u)) 
				{
				  // errs () << "skipping PHI use of " 
				  // << *inst << "\n";
				  continue;
				}
			    }
			  keep = true; 
			  break;
			}
		    }
		  if (!keep) 
		    { 
		      env.erase (it++);
		      continue;
		    }
		}
	    }
	  // -- didn't erase the iterator, do pre-increment
	  ++it;
	}
    }    
  };

  template <typename Graph>
  class VCEncoder
  {
  private:
    typedef graph_traits<ARG> Traits;
    typedef typename Traits::vertex_descriptor Vertex;
    typedef typename Traits::edge_descriptor Edge;

  public:
    /**
     * Compute VC of an ArgCond
     *
     * VC is a collection of formulas that is satisfiable iff there
     * exists a path through the graph g such that the conjunction of
     * edge labels on the path is satisfiable.
     *
     * \tparam Range a boost::range
     * \tparam EdgeMap a property map frome Edges to Expr
     * \tparam OutputIterator STL OutputIterator
     * 
     * \param[in] efac Expression factory
     * \param[in] g the input graph
     * \param[in] topo topological order of vertices of g
     * \param[in] argCond a property map from edges of g to Expr
     * \param[out] vc an output iterator to store generated VC
     * 
     */
    template <typename Range, 
	      typename EdgeMap, typename OutputIterator>
    static void encodeVC (ExprFactory &efac, 
			  const Graph &g, 
			  Range &topo, 
			  const EdgeMap &argCond, 
			  OutputIterator vc)
    {
      // -- for each vertex in topological order
      foreach (Vertex v, topo)
	{
	  // -- variable for the node
	  Expr vTerm = mkTerm (v, efac);

	  // -- partial result, one entry per each edge
	  ExprVector subRes;
	  // -- variables for the edges for the mutex constraint
	  ExprVector edgVars;

	  // -- for each edge
	  foreach (Edge uc, out_edges (v, g))
	    {
	      
	      // -- variable for the child node 'c'
	      Expr cTerm = mkTerm (target (uc, g), efac);
	      
	      // -- edge condition, depends on child node
	      Expr edgeC = get (argCond, uc);
	      // -- skip resolvers if they are there
	      if (isOpX<TUPLE> (edgeC)) edgeC = edgeC->left ();
	      
	      edgeC = boolop::land (cTerm, edgeC);
	      
	      // -- allocate variable for the edge (used later)
	      Expr ev = mkEdgVar (vTerm, cTerm);
	      edgeC = boolop::land (edgeC, ev);
	      edgVars.push_back (ev);

	      
	      // -- store current edge condition
	      subRes.push_back (edgeC);
	    }

	  // -- result is a disjunction of all edge conditions
	  Expr res = mknary<OR> (mk<FALSE> (efac), 
				 subRes.begin (),
				 subRes.end ());
	  
	  // -- assume there is only one leaf node, the exit node of
	  // -- the DAG. It is labeled by true: if we get there then
	  // -- there is a counterexample
	  if (isLeaf (v, g)) res = vTerm;
	  else 
	    {
	      // -- conditional on the vertex variable if not root
	      if (!isRoot (v, g)) res = boolop::limp (vTerm, res);

	      // -- exactly one edge from v can be active 
	      Expr mex = exactlyOne (edgVars, efac);
	      if (!isRoot (v, g)) mex = boolop::limp (vTerm, mex);

	      res = boolop::land (res, mex);
	    }


	  // -- placeholder for resolve expression at this vertex
	  Expr resolveE = mk<TRUE> (efac);
	  // -- for each incoming edge
	  foreach (Edge pv, in_edges (v, g))
	    {
	      Expr pTerm = mkTerm (source (pv, g), efac);
	      // -- if eCond of pv has a resolver, grab it
	      Expr eCond = get (argCond, pv);
	      if (!isOpX<TUPLE> (eCond)) continue;
	      if (isOpX<TRUE> (eCond->right ())) continue;
	      
	      resolveE = boolop::land (resolveE, 
				       boolop::limp (mkEdgVar (pTerm, vTerm),
						     eCond->right ()));
	    }

	  if (!isOpX<TRUE> (resolveE))
	    {
	      // -- store into previous result
	      --vc;
	      *vc = boolop::land (*vc, resolveE);
	      ++vc;
	    }

	  *vc = res;
	  ++vc;
	}
    }    
  };

  template <typename Graph, typename Range, 
	    typename EdgeMap, typename OutputIterator>
  static void encodeVC (ExprFactory &efac, 
			const Graph &g, Range &topo, const EdgeMap &argCond, 
			OutputIterator vc)
  {
    VCEncoder<Graph>::encodeVC (efac, g, topo, argCond, vc);
  }
  
  
  /** 
   * Constructs a counter-example -- a sequence of basic blocks 
   * (ufo::Node*) given a Model
   * 
   * \tparam Graph a BGL graph for the Arg
   * \tparam Model a model that can evaluate terms to their values
   *
   */
  template<typename Graph, typename Smt> 
  class ArgCexBuilder
  {
    typedef graph_traits<Graph> Traits;
    typedef typename Traits::vertex_descriptor Vertex;
    typedef typename Traits::edge_descriptor Edge;

    const Graph &g;
    Vertex en;
    Vertex ex;
    Smt &m;
    
    NodeList res;

  public:
    ArgCexBuilder (const Graph &arg, Vertex entry, Vertex exit, Smt &smt):
      g(arg), en(entry), ex(exit), m(smt) { assert (m.hasModel ()); }


    template <typename OutputIterator>
    void build (OutputIterator out)
    {
      Vertex v;
      v = en;
      *(out++) = v;
      while (v != ex)
	{
	  Edge e;
	  forall (e, out_edges (v, g))
	    {
	      Expr eTerm = mkEdgVar (m.getFactory (), e);
	      Expr eVal = m.getModelValue (eTerm);
	      
	      if (isOpX<TRUE> (eVal))
		{
		  v = target (e, g);
		  *(out++) = v;
		  break;
		}
	    }
	  assert (v == target (e, g));
	}
    }    
  };  
  
  template <typename Graph, typename Smt, typename OutputIterator>
  void doArgCex (const Graph &g, 
	       typename graph_traits<Graph>::vertex_descriptor en,
	       typename graph_traits<Graph>::vertex_descriptor ex,
	       Smt &smt,
	       OutputIterator out)
  {
    ArgCexBuilder<Graph,Smt> b (g, en, ex, smt);
    b.build (out);
  }
}

#endif
