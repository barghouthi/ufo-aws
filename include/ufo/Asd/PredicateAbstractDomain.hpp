#ifndef __PRED_ABS_H_
#define __PRED_ABS_H_

#include "ufo/ufo.hpp"
#include "ufo/Asd/AbstractStateDomain.hpp"

#include "ufo/Smt/ExprZ3.hpp"

// -- Predicate Abstraction
namespace ufo
{
  
  namespace paasd
  {    
    struct OverPredicates
    {
      VisitAction operator() (Expr exp) const
      { 
	if (bind::isBoolVar (exp)) 
	  return VisitAction::changeTo (bind::name (exp));
	return VisitAction::doKids ();
      }
    };
    
      

    // -- This visitor collects predicates from a given formula
    // -- and adds them to preds;
    template <typename T>
    struct CollectPredicates
    {
      CutPointPtr cp;
      BoolExprFn &filter;
      
      T &predabs;

      CollectPredicates(CutPointPtr c, BoolExprFn &f, T &p): 
	cp(c), filter(f), predabs(p) {}

      VisitAction operator() (Expr exp)
      {
	if (isOpX<TRUE> (exp) || isOpX<FALSE> (exp)) 
	  return VisitAction::skipKids ();
	
	if (isOp<BoolOp> (exp)) return VisitAction::doKids();

	if (filter.apply (exp)) predabs.addPred (cp, exp);

	return VisitAction::skipKids ();
      }
    };

  }

  using namespace ufo;

  class PredicateAbstractStateValue : public AbstractStateValue
  {
  protected:
    Expr val;

  public:
    PredicateAbstractStateValue (Expr v) : val(v) {}
    
    Expr getVal () const { return val; }
  };
    

  /** Base for Predicate Abstraction. A predicate abstraction is a map
      from CutPoints to Predicates and a Post Image function*/
  class PredicateAbstractDomain : public AbstractStateDomain
  {
  protected:
    /** Large Block Encoding */
    LBE& lbe;

    ExprFactory& efac;

    // -- helper constants
    Expr trueE;
    Expr falseE;
      
    // -- for now will assume only MSAT is passable
    ExprMSat& ms;			

    BoolExprFn *filter;

  public:
    PredicateAbstractDomain (LBE& lbePass, ExprFactory& e, ExprMSat& m, 
			     BoolExprFn *f = new TrueBoolExprFn ()) : 
      lbe(lbePass), efac(e), trueE(mk<TRUE>(efac)), falseE(mk<FALSE>(efac)), 
      ms(m), filter (f) {}

    virtual bool isFinite () { return true; }
    std::string name () { return "NULLPA"; }
    
    virtual AbstractState alpha (CutPointPtr loc, Expr val) 
    { 
      return absState (val); 
    }
    
    virtual Expr gamma (CutPointPtr loc, AbstractState v)
    {
      return getExpr (v);
    }

    virtual AbstractState top (CutPointPtr loc) 
    {
      return absState (trueE);
    }
    virtual AbstractState bot (CutPointPtr loc)
    {
      return absState (falseE);
    }

    virtual bool isTop (CutPointPtr loc, AbstractState v)
    {
      Expr e = getExpr (v);
      return e == trueE || isSAT (boolop::lneg (e)) == false;
    }

    virtual bool isBot (CutPointPtr loc, AbstractState v)
    {
      // BOT == UNSAT
      Expr e = getExpr (v);
      return e == falseE || isSAT (e) == false;
    }
    
    virtual bool isLeq (CutPointPtr loc, AbstractState v1, AbstractState v2)
    {
      Expr e1 = getExpr (v1);
      Expr e2 = getExpr (v2);
      return isSAT (boolop::land (e1, boolop::lneg (e2))) == false;
    }
    
    virtual bool isLeq (CutPointPtr loc, AbstractState v, 
			AbstractStateVector const &u)
    {
      Expr c = trueE;
      foreach (AbstractState s, u)
	c = boolop::land (c, boolop::lneg (getExpr (s)));
      
      Expr e = getExpr (v);
      return isSAT (boolop::land (e, c)) == false;
    }
    
    virtual bool isEq (CutPointPtr loc, AbstractState v1, AbstractState v2)
    {
      Expr e1 = getExpr (v1);
      Expr e2 = getExpr (v2);
      
      return isSAT 
	(boolop::lor (boolop::land (e1, boolop::lneg (e2)),
		      boolop::land (e2, boolop::lneg (e1)))) == false;
    }
    
    virtual AbstractState join (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return absState (boolop::lor (getExpr (v1), getExpr (v2)));
    }

    virtual AbstractState meet (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return absState (boolop::land (getExpr (v1), getExpr (v2)));
    }

    virtual AbstractState widen (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      return v2;
    }

    virtual AbstractState widenWith (CutPointPtr loc, 
				     const AbstractStateVector &newV,
				     AbstractState oldV)
    {
      return oldV;
    }
    
    
    virtual AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst)
    {
      edgeIterator it = src->findSEdge (*dst);
      assert (it != src->succEnd ());
      
      return absState (alphaPost (getExpr (pre), *it));
    }

    virtual AbstractState post (const LocLabelStatePairVector &pre,
				Loc dst, 
				BoolVector &deadLocs)
    {
      Expr res = falseE;

      
      size_t count = 0;
      foreach (LocLabelStatePair lv, pre)
	{
	  edgeIterator it = lv.first->findSEdge (*dst);
	  assert (it != lv.first->succEnd ());
	  
	  Expr u = lv.second.first;
	  if (!u) u = getExpr (lv.second.second);
	  Expr x = alphaPost (u, *it);
	  
	  if (x == falseE) deadLocs [count] = true;
	  else res = boolop::lor (res, x);
	  count++;
	  
	} 

      return absState (res);
    }


    virtual void setRefinementFilter (BoolExprFn *f)
    {
      delete filter;
      filter = f;
    }
    
    virtual void refinementHint (CutPointPtr loc, Expr hint)
    {
      paasd::CollectPredicates<PredicateAbstractDomain> cp (loc, 
							     *filter, *this);
      // Light refinement:
      // CollectPredsFromInterp cpi (n->getLoc (), predDom); 
      
      dagVisit (cp, hint);
    }
    
  protected:
    AbstractState absState (Expr e)
    {
      return AbstractState (new PredicateAbstractStateValue (e));
    }

    Expr getExpr (AbstractState v)
    {
      return dynamic_cast<PredicateAbstractStateValue*>(&*v)->getVal ();
    }

  public:
    
    /** Returns the post image of pre over the edge */
    virtual Expr alphaPost (Expr pre, SEdgePtr edge) 
    { 
      return pre == falseE ? falseE : trueE; 
    }
		
    /** Adds a predicate to a cut point */
    void addPred (CutPointPtr cp, Expr p) { cp2preds[cp].insert (p); }

    /** Returns predicates for a given cutpoint */
    ExprSet& getPreds(CutPointPtr cp) { return cp2preds[cp]; }

    /** Forgets all predicates */
    void reset () { cp2preds.clear(); }

  private:
    /** map from CutPoints to Predicates */
    std::map<CutPointPtr,std::set<Expr> > cp2preds;

  protected:

    /** checks satisfiability of expression with the current theorem prover */
    boost::tribool isSAT(Expr e) //{ return ms.isSAT(e); }
    {
      return z3_is_sat (e);
    }
    
  public:
    virtual ~PredicateAbstractDomain () { delete filter; }
  };

  class CartesianPADomain : public PredicateAbstractDomain
  {
  public:

    CartesianPADomain (LBE& l, ExprFactory& e, ExprMSat& m) : 
      PredicateAbstractDomain (l, e, m) {}

    std::string name () { return "CPRED"; }
    
    virtual Expr alphaPost (Expr pre, SEdgePtr edge)
    {

      Environment env (efac);
      Expr ant = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);
      Expr result = trueE;

      ExprSet& dstPreds = getPreds (edge->getDst ());

      if (dstPreds.empty())
	{
	  boost::tribool sat = isSAT(ant);
	  if (sat) 
	    return trueE;
	  else if (maybe(sat))
	    {
	      assert(0 && "\nMAYBE\n");
	      return trueE;
	    }
	  else 
	    return falseE;
	}
      
      // -- !q.preds.empty ()
      foreach (Expr p, dstPreds)
	{
	  // -- p in the environment
	  Expr envP = env.eval (p);
	 
	  if(!isSAT (mk<AND> (ant, boolop::lneg (envP))))
	    result = boolop::land (result, p);
	  else if(!isSAT (mk<AND> (ant, (envP))))
	    result = boolop::land (result, boolop::lneg (p));
	}
      return result;
    }

  };

  class BoolPADomain : public PredicateAbstractDomain
  {
  public:

    BoolPADomain (LBE& l, ExprFactory& e, ExprMSat& m) : 
      PredicateAbstractDomain (l, e, m) {}

    std::string name () { return "BPRED"; }

    virtual Expr alphaPost (Expr pre, SEdgePtr edge)
    {
      Environment env (efac);
      // -- predicate abstraction query
      Expr paq = SEdgeCondComp::computeEdgeCond (efac, env, pre, edge, lbe);

      ExprSet &dstPreds = getPreds (edge->getDst ());

      if (dstPreds.empty ())
	{
	  boost::tribool sat = isSAT(paq);
	  if (sat) return trueE;
	  else if (maybe(sat))
	    {
	      assert(0 && "\nMAYBE\n");
	      return trueE;
	    }
	  else return falseE;
	}
        
      ExprVector important;

      /** enumerate predicates: required for STRING encoding :( */
      ExprVector preds (dstPreds.begin (), dstPreds.end ());
	
      for(size_t i = 0; i < preds.size (); i++)
	{
	  Expr pred = bind::boolVar (preds[i]);
	  Expr pdef = mk<IFF>(env.eval (preds [i]), pred);
	  paq = mk<AND>(paq, pdef);
          important.push_back(pred);
        }
              
      Expr result = allSAT (paq, important);

      // -- unwrap the result to be over original destination predicates
      paasd::OverPredicates op;
      return dagVisit (op, result);
    }
  private:
    Expr allSAT (Expr e, const ExprVector &terms)
    {
      return ms.allSAT (e, terms);
      //return z3_all_sat (e, terms);
    }
    
  };

  /** only picks expressions that correspond to intervals */
  struct IntervalsRefFilter : BoolExprFn
  {
    bool apply (Expr e)
    {
      Expr lhs = e->left();
      Expr rhs = e->right();

      return ((isOpX<GEQ>(e) || isOpX<LEQ>(e) 
	       || isOpX<GT>(e) || isOpX<LT>(e) 
	       || isOpX<EQ>(e) || isOpX<NEQ>(e)) 
	      && ((isOpX<MPQ>(lhs) && isOpX<VALUE>(rhs)) || 
		  (isOpX<MPQ>(rhs) && isOpX<VALUE>(lhs))));
      
    }
  };
    

}

#endif
