/**
 * Boolean Abstract State Domain.
 * Using Interval as the underlying representation for Boolean variables
 * TRUE [+oo,+oo]
 * FALSE [-oo,-oo]
 * TOP [-oo,+oo]
 * BOT [+oo,-oo]
 */
#ifndef __BOOL_ABSTRACT_STATE_DOMAIN_HPP_
#define __BOOL_ABSTRACT_STATE_DOMAIN_HPP_

#include "ufo/Cpg/Lbe.hpp"
#include "AbstractStateDomain.hpp"
#include "Interval.hpp"

namespace ufo
{
  typedef map<Expr, Interval> BoolState;

  /**
     Boolean Abstract State Value
   */
  class BoolAbstractStateValue : public AbstractStateValue
  {
  private:
    BoolState val;
  public:
    BoolAbstractStateValue (BoolState _s) : val(_s) {}

    BoolState& getVal()
    {
      return val;
    }
  };

  /**
     Boolean Abstract State Domain
   */
  class BoolAbstractStateDomain : public AbstractStateDomain
  {
  public:
    // Disable default copy constructor
    BoolAbstractStateDomain (const BoolAbstractStateDomain&);
    BoolAbstractStateDomain (ExprFactory &_efac, const ExprSet &_v) : 
      efac(_efac), z3(efac, StringMap(), true), vars(_v) {}
    

    ~BoolAbstractStateDomain () {}

  private:
    ExprFactory &efac;
    ExprZ3 z3;
    const ExprSet &vars; // tracked variables

  public:
    std::string name () {return "Boolean Abstract Domain";}

    /**
       abs: create an abstract state from BoolState map
     */
    AbstractState abs (BoolState _s)
    {
      return AbstractState (new BoolAbstractStateValue (_s));
    }

    /**
       getVars: return tracked variables in the domain
    */
    const ExprSet& getVars () { return vars; }
    
    /**
       getVal: return a reference to the BoolState map
     */
    BoolState& getVal (AbstractState _a)
    {
      assert (_a && "Abstract State is NULL");
      return dynamic_cast<BoolAbstractStateValue*>(&*_a)->getVal();
    }

    /**
       point2state.
       param[in] Range (map) from dimensions to values
       return an abstract state
     */
    template <typename R>
    AbstractState point2state (R &bm)
    {
      BoolState s;
      typedef pair<Expr,Expr> ExprPair;
      
      foreach (ExprPair p, bm)
        {
          if (isOpX<TRUE> (p.second))
            s[p.first] = Interval (posInf(), posInf());
          else if (isOpX<FALSE> (p.second))
            s[p.first] = Interval (negInf(), negInf());
          else
            s[p.first] = Interval::top();
        }
      return abs (s);
    }

    /**
       alpha function
    */
    AbstractState alpha (CutPointPtr loc, Expr val)
    {
      BoolState s;
      foreach (Expr _v, vars)
        {
          s[_v] = boundBool(val, _v);
        }
      return abs (s);
    }

    /**
       Concretize an abstract state with respect to a given location
    */
    Expr gamma (CutPointPtr loc, AbstractState v)
    {
      BoolState& s = getVal (v);
      ExprVector g;
      foreach (Expr _v, vars)
        {
          assert (s.count(_v) > 0);
          g.push_back (s[_v].gamma(_v));
        }
      return mknary<AND>(mk<TRUE>(efac), g.begin(), g.end());
    }

    /**
      top function
    */
    AbstractState top (CutPointPtr loc)
    {
      BoolState s;
      foreach (Expr _v, vars)
        {
          s[_v] = Interval (posInf(), posInf());
        }
      return abs (s);
    }

    /**
       bottom function
    */
    AbstractState bot (CutPointPtr loc)
    {
      BoolState s;
      foreach (Expr _v, vars)
        {
          s[_v] = Interval (negInf(), negInf());
        }
      return abs (s);
    }

    bool isTop (CutPointPtr loc, AbstractState v)
    {
      BoolState& s = getVal (v);
      foreach (Expr _v, vars)
        {
          assert (s.count (_v) > 0);
          if (!isTrue(s[_v])) return false;
        }
      return true;
    }

    bool isBot (CutPointPtr loc, AbstractState v)
    {
      BoolState& s = getVal (v);
      foreach (Expr _v, vars)
        {
          assert (s.count (_v) > 0);
          if (isFalse(s[_v])) return true;
        }
      return false;
    }
    
    /** Returns true iff v1 is a subset of v2 */
    bool isLeq (CutPointPtr loc, 
                AbstractState v1, AbstractState v2)
    {
      assert (0 && "unimplemented");
      return false;
    }

    /** Returns true iff v1 is a subset of UNION (x | x \in u) */
    bool isLeq (CutPointPtr loc, AbstractState v1, 
                AbstractStateVector const &u)
    {
      assert (0 && "unimplemented");
      return false;
    }

    AbstractState copy (AbstractState _x)
    {
      BoolState& b = getVal (_x);
      BoolState c = BoolState (b);

      return abs (c);
    }

    virtual bool isEq (CutPointPtr loc, AbstractState v1, AbstractState v2)
    {
      assert (0 && "unimplemented");
      return false;
    }
    
    virtual AbstractState join (CutPointPtr loc, 
				AbstractState v1, AbstractState v2)
    {
      BoolState s;
      BoolState& s1 = getVal (v1);
      BoolState& s2 = getVal (v2);

      foreach (Expr _v, vars)
        {
          assert (s1.count(_v)>0);
          assert (s2.count(_v)>0);
          assert (s1.size() == s2.size());
          s[_v] = s1[_v].join(s2[_v]);
        }
      return abs (s);
    }

    AbstractState meet (CutPointPtr loc, 
                        AbstractState v1, AbstractState v2)
    {
      BoolState s;
      BoolState& s1 = getVal (v1);
      BoolState& s2 = getVal (v2);

      foreach (Expr _v, vars)
        {
          assert (s1.count(_v)>0);
          assert (s2.count(_v)>0);
          assert (s1.size() == s2.size());
          s[_v] = s1[_v].meet(s2[_v]);
        }
      return abs (s);
    }

    AbstractState widen (CutPointPtr loc, 
                         AbstractState v1, AbstractState v2)
    {
      BoolState s;
      BoolState& s1 = getVal (v1);
      BoolState& s2 = getVal (v2);

      foreach (Expr _v, vars)
        {
          assert (s1.count(_v)>0);
          assert (s2.count(_v)>0);
          assert (s1.size() == s2.size());
          s[_v] = s1[_v].widen(s2[_v]);
        }
      return abs (s);
    }

    AbstractState post (AbstractState pre, 
                        CutPointPtr src, CutPointPtr dst)
    {
      assert (0 && "Unimplemented");
      BoolState s;
      return abs (s);
    }

    /** Called to hint the abstract domain that it is useful to be
	able to preciiselly represent hint expression at a given
	location*/
    void refinementHint (CutPointPtr loc, Expr hint)
    {
      assert (0 && "Unimplemented");
    }
    
    bool isFinite ()
    {
      return true;
    }

    AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
                             AbstractState newV)
    {
      assert (0 && "Unimplemented");
      BoolState s;
      return abs (s);
    }
	
    AbstractState post (const LocLabelStatePairVector &pre, 
                        Loc dst,
                        BoolVector &deadLocs)
    {
      assert (0 && "Unimplemented");
      BoolState s;
      return abs (s);
    }

    /**
       getBound.
       param[in] Expr _name: variable name
       param[in] AbstractState _s: state
       return value of _name in state
    */
    Interval getBound (Expr _name, AbstractState _s)
    {
      BoolState& s = getVal (_s);
      assert (s.count (_name) > 0);
      return s[_name];
    }

    void print (AbstractState _s)
    {
      errs () << "Bool State:\n";
      BoolState& s = getVal (_s);
      foreach (Expr _v, vars)
        {
          assert (s.count(_v) > 0);
          errs () << *_v << " : [" << s[_v].first << "," << s[_v].second 
                  << "]\n";
        }
    }

  private:
    /**
       isBool. Check if a variable is Boolean type.
       param[in] Expr var
       return bool
    */
    static bool isBoolVar (Expr var)
    {
      Expr u = var;
      if (isOpX<NEG>(u)) u = u->left();
      if (!(isOpX<VALUE>(u) || isOpX<VARIANT>(u)))
        return false;
      
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (!isOpX<VALUE>(u)) return true;

      if (isBoolType(getTerm<const Value*>(u)->getType()))
        return true;

      return false;
    }

    Interval boundBool(Expr e, Expr var)
    {
      Interval res = Interval::bot();
      z3.push();
      z3.assertExpr(var); // assert x.
      if (z3.solve())
        res.second = posInf();
      z3.pop();
      z3.push();
      z3.assertExpr(mk<NEG>(var)); // assert not x.
      if (z3.solve())
        res.first = negInf();
      z3.pop();

      return res;
    }
    
    bool isTrue (Interval &itv)
    {
      return isPosInf(itv.first) && isPosInf(itv.second);
    }
    
    bool isFalse (Interval &itv)
    {
      return isNegInf(itv.first) && isNegInf(itv.second);
    }
  };
}

#endif
