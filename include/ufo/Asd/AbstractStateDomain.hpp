#ifndef __ABSTRACT_STATE_DOMAIN__HPP_
#define __ABSTRACT_STATE_DOMAIN__HPP_

#include "ufo/ufo.hpp"
#include "ufo/Cpg/Lbe.hpp"

namespace ufo
{
  class AbstractStateValue
  {
  public:
    // XXX add Print method
    virtual ~AbstractStateValue () {}
  };


  typedef CutPointPtr Loc;
  typedef boost::shared_ptr<AbstractStateValue> AbstractState;

  typedef std::vector<AbstractState> AbstractStateVector;

  typedef std::pair<Loc,AbstractState> LocStatePair;
  typedef std::vector<LocStatePair> LocStatePairVector;
  
  typedef std::pair<Expr,AbstractState> LabelStatePair;
  typedef std::pair<Loc,LabelStatePair> LocLabelStatePair;
  typedef std::vector<LocLabelStatePair> LocLabelStatePairVector;

  typedef std::vector<bool> BoolVector;


  class AbstractStateDomain
  {
  public:
    virtual ~AbstractStateDomain () {}
    
    /** Returns the name of the abstract domain */
    virtual std::string name () { return "none"; }
    
    /** Abstract an expression by an AbstractState at a given location */
    virtual AbstractState alpha (CutPointPtr loc, Expr val) = 0;
    /** Concretize an abstract state with respect to a given location */
    virtual Expr gamma (CutPointPtr loc, AbstractState v) = 0;

    virtual AbstractState top (CutPointPtr loc) = 0;
    virtual AbstractState bot (CutPointPtr loc) = 0;

    virtual bool isTop (CutPointPtr loc, AbstractState v) = 0;
    virtual bool isBot (CutPointPtr loc, AbstractState v) = 0;
    
    /** Returns true iff v1 is a subset of v2 */
    virtual bool isLeq (CutPointPtr loc, 
			AbstractState v1, AbstractState v2) = 0;
    /** Returns true iff v1 is a subset of UNION (x | x \in u) */
    virtual bool isLeq (CutPointPtr loc, AbstractState v1, 
			AbstractStateVector const &u) = 0;
    virtual bool isEq (CutPointPtr loc, AbstractState v1, AbstractState v2) = 0;
    

    virtual AbstractState join (CutPointPtr loc, 
				AbstractState v1, AbstractState v2) = 0;
    virtual AbstractState meet (CutPointPtr loc, 
				AbstractState v1, AbstractState v2) = 0;
    virtual AbstractState widen (CutPointPtr loc, 
				 AbstractState v1, AbstractState v2) = 0;

    virtual AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst) = 0;

    /** Called to hint the abstract domain that it is useful to be
	able to preciiselly represent hint expression at a given
	location*/
    virtual void refinementHint (CutPointPtr loc, Expr hint) = 0;

    /** true if this abstract domain is finite height */
    virtual bool isFinite () = 0;
    virtual AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
				     AbstractState newV) = 0;

    /** Given a sequence pre of (Loc,Value) pairs, computes an
	over-approximation of the post image: 
	OR_{(l,v) \in pre} post (v, l, dst)
	
	At the end, deadLocs[i] == true iff for the ith element
	(li,vi) of pre post(v,l,dst) = bot

	deadLocks must be pre-allocated by the caller
    */	
    virtual AbstractState post (const LocLabelStatePairVector &pre, 
				Loc dst,
				BoolVector &deadLocs) = 0;

    /*
    template<typename InputIterator, typename OutputIterator>
    virtual AbstractState post (std::pair<InputIterator,InputIterator> pre, 
				CutPointPtr dst, OutputIterator falseEdges) = 0;
    */
  };

  class ProductAbstractStateDomain : public AbstractStateDomain
  {
  protected:
    typedef std::pair<AbstractState,AbstractState> AbsStatePair;
    
    struct AbsVal : public AbstractStateValue
    {
      AbsStatePair val;
      
      AbsVal (AbstractState l, AbstractState r) : val(l,r) { }
      AbsVal (AbsStatePair const &v) : val(v) {}
      
    };

    AbstractStateDomain &lhsDom;
    AbstractStateDomain &rhsDom;
  public:
    ProductAbstractStateDomain (AbstractStateDomain &l,
				AbstractStateDomain &r) : 
      lhsDom (l), rhsDom (r) {}
    
    std::string name () { return lhsDom.name () + " x " + rhsDom.name (); }
    
    virtual void refinementHint (Loc loc, Expr hint)
    {
      lhsDom.refinementHint (loc, hint);
      rhsDom.refinementHint (loc, hint);
    }
    
    virtual bool isFinite () 
    { 
      return lhsDom.isFinite () && rhsDom.isFinite ();
    }
    
    virtual AbstractState alpha (Loc loc, Expr val)
    {
      return absState (lhsDom.alpha (loc, val), rhsDom.alpha (loc, val));
    }

    virtual Expr gamma (Loc loc, AbstractState s)
    {
      AbsStatePair &v = getAbsVal (s);
      return boolop::land (lhsDom.gamma (loc, v.first), 
			   rhsDom.gamma (loc, v.second));
      
    }
    
    virtual AbstractState top (CutPointPtr loc)
    {
      return absState (lhsDom.top (loc), rhsDom.top (loc));
    }
    virtual AbstractState bot (CutPointPtr loc)
    {
      return absState (lhsDom.bot (loc), rhsDom.bot (loc));
    }
    virtual bool isTop (Loc loc, AbstractState s)
    {
      AbsStatePair &v = getAbsVal (s);
      return lhsDom.isTop (loc, v.first) && rhsDom.isTop (loc, v.second);
    }
    virtual bool isBot (Loc loc, AbstractState s)
    {
      AbsStatePair &v = getAbsVal (s);
      return lhsDom.isBot (loc, v.first) || rhsDom.isBot (loc, v.second);
    }

    virtual bool isLeq (CutPointPtr loc, AbstractState s1, AbstractState s2) 
    {
      AbsStatePair &v1 = getAbsVal (s1);
      AbsStatePair &v2 = getAbsVal (s2);
      return lhsDom.isLeq (loc, v1.first, v2.first) &&
	rhsDom.isLeq (loc, v1.second, v2.second);
    }
    virtual bool isLeq (CutPointPtr loc, AbstractState s, 
			AbstractStateVector const &u)
    {
      AbsStatePair &v = getAbsVal (s);
      
      AbstractStateVector u1;
      AbstractStateVector u2;

      foreach (const AbstractState &x, u)
	{
	  AbsStatePair &y = getAbsVal (x);
	  u1.push_back (y.first);
	  u2.push_back (y.second);
	}	  

      return lhsDom.isLeq (loc, v.first, u1) && 
	lhsDom.isLeq (loc, v.second, u2);
    }
    
    virtual bool isEq (CutPointPtr loc, AbstractState s1, AbstractState s2) 
    {
      AbsStatePair &v1 = getAbsVal (s1);
      AbsStatePair &v2 = getAbsVal (s2);
      return lhsDom.isEq (loc, v1.first, v2.first) &&
	rhsDom.isEq (loc, v1.second, v2.second);
    }
    
    virtual AbstractState meet (CutPointPtr loc, 
				AbstractState s1, AbstractState s2) 
    {
      AbsStatePair &v1 = getAbsVal (s1);
      AbsStatePair &v2 = getAbsVal (s2);
      return absState (lhsDom.meet (loc, v1.first, v2.first), 
		       rhsDom.meet (loc, v1.second, v2.second));
    }

    virtual AbstractState join (CutPointPtr loc, 
				AbstractState s1, AbstractState s2) 
    {
      AbsStatePair &v1 = getAbsVal (s1);
      AbsStatePair &v2 = getAbsVal (s2);
      return absState (lhsDom.join (loc, v1.first, v2.first), 
		       rhsDom.join (loc, v1.second, v2.second));
    }
    
    virtual AbstractState widen (CutPointPtr loc, 
				AbstractState s1, AbstractState s2) 
    {
      AbsStatePair &v1 = getAbsVal (s1);
      AbsStatePair &v2 = getAbsVal (s2);
      return absState (lhsDom.widen (loc, v1.first, v2.first), 
		       rhsDom.widen (loc, v1.second, v2.second));
    }
    
    virtual AbstractState widenWith (Loc loc, AbstractStateVector const &oldV, 
				     AbstractState newV)
    {
      AbsStatePair &v = getAbsVal (newV);
      AbstractStateVector u1;
      AbstractStateVector u2;

      foreach (const AbstractState &x, oldV)
	{
	  AbsStatePair &y = getAbsVal (x);
	  u1.push_back (y.first);
	  u2.push_back (y.second);
	}

      return absState (lhsDom.widenWith (loc, u1, v.first),
		       rhsDom.widenWith (loc, u2, v.second));
    }
    
    virtual AbstractState post (AbstractState pre, 
				CutPointPtr src, CutPointPtr dst)
    {
      AbsStatePair &v = getAbsVal (pre);      
      return absState (lhsDom.post (v.first, src, dst),
		       rhsDom.post (v.second, src, dst));
    }
    

    
    virtual AbstractState post (const LocLabelStatePairVector &pre,
				Loc dst, 
				BoolVector &deadLocs)
    {
      LocLabelStatePairVector pre1;
      LocLabelStatePairVector pre2;
      foreach (LocLabelStatePair lv, pre) 
	{
	  Expr &u = lv.second.first;
	  AbsStatePair v;
	  if (lv.second.second) v = getAbsVal (lv.second.second);

	  pre1.push_back (std::make_pair (lv.first, 
					  std::make_pair(u, v.first)));
	  pre2.push_back (std::make_pair (lv.first, 
					  std::make_pair(u, v.second)));

	}
	    
      return absState (lhsDom.post (pre1, dst, deadLocs),
		       rhsDom.post (pre2, dst, deadLocs));
    }
    

    
  protected:
    inline AbstractState absState (AbstractState l, AbstractState r)
    {
      assert (l && r);
      return AbstractState (new AbsVal (l, r));
    }
    
    inline AbsStatePair &getAbsVal (AbstractState v)
    {
      return dynamic_cast<AbsVal*>(&*v)->val;
    }    
  };
}


#endif
