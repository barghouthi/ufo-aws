#ifndef __ABS_ANALYSIS_H_
#define __ABS_ANALYSIS_H_

#include "boost/shared_ptr.hpp"


// -- Abstract Interpreter
namespace ufo
{
  
  /** Computes an abstract post over a given summary edge, 
      parametrized by type of abstract values V, and an abstract domain T
  **/
  template <typename V, typename T>
  class SEdgeAbsPost
  {
  private:
    /** Abstract domain */
    T &domain;    

    /** pre-condition for the post-image */
    V pre;
    /** the summary edge */
    SEdgePtr edge;
    

    /** convinience */
    const BasicBlock *srcBB;
      
    /** type of preMap */
    typedef std::map<const BasicBlock*, V> pre_map_type;
    /** Maps a BB to current pre-condition.  The result of
	computePost() is stored in preMap[edge->getDst ()->getBB
	()]. Note that if the edge is a self-loop, then src and dst
	share a basic block. Hence, pre-image of the src BB is stored
	in the field 'pre' and not in the map.
     */
    pre_map_type preMap;

  public:
    SEdgeAbsPost(T &dom, V p, SEdgePtr sedge) : 
      domain(dom), pre(p), edge (sedge),
      srcBB (sedge->getSrc ()->getBB ()) {}

    /** Returns the pre-condition for the image computation */
    V getPre () const { return pre; }
    
    /** Returns the post-condition for the image computation. Only
	available after the computation was done. */
    V getPost () const
    {
      return preMap.find (edge->getDst ()->getBB ())->second;
    }
    
      
    /** Runs post computation */
    V computePost ()
    {
      // -- basic blocks are arranged in topological
      // -- order in edge
      foreach (const BasicBlock *bb, *edge)
	{
	  
	  // -- skip srcBB, it's pre-condition is given
	  if (bb == srcBB) continue;
	  processBB (bb);
	}
      
      /** process PHI-nodes of the destination BB */
      processBB (edge->getDst ()->getBB ());

      return getPost ();
    }

  private:
    /** get current pre-image for a basic block */
    V getPre (const BasicBlock *bb) const
    {
      return bb == srcBB ? pre : preMap.find (bb)->second;
    }
 
    /** computes the pre-image of a given BB by computing post-image
	of all of the predecessor BBs */
    void processBB(const BasicBlock* bb)
    {

      /** start with bottom */
      preMap [bb] = domain.bot ();
	
      for (const_pred_iterator PI = pred_begin(bb), 
	     E = pred_end(bb); PI != E; ++PI)
	{
	  
	  const BasicBlock* pred = *PI;
	  // -- skip predecessors that are not on this SEdge
	  if (pred != srcBB && preMap.count(pred) <= 0) continue;

	  // -- compute post-image of one edge 
	  V res = 
	    postPhi (postImage (getPre (pred), pred, bb), pred, bb);

	  // -- log into preMap
	  preMap [bb] = domain.join (preMap [bb], res);
	}
    }   

    /** Compute post-image over all instructions in src on the way to dst */
    V postImage (V pre, const BasicBlock *src, const BasicBlock *dst) const
    {
      V res = pre;
      assert (res != NULL);

      // -- process basic block instructions sequentially
      //foreach (const Instruction &inst, *src)
      // res = postImage (res, &inst, dst);

      // XXX Modified for osx.
     for(BasicBlock::const_iterator j = src->begin(),
            je = src->end(); j != je; ++j)
	res = postImage (res, &(*j), dst);
#ifdef LDD_DEBUG_VERBOSE
      errs () << "POST OVER BB " 
	      << *src << "of \n"
	      << *(domain.gamma (pre)) << "is \n"
	      << *(domain.gamma (res)) << "\n"
	      << "END POST BB\n";
#endif
      return res;
    }
 

    /** compute post-image over the PHI-nodes of bb from a given src */
    V postPhi (V pre, const BasicBlock *src, const BasicBlock *bb) const
    {
      V res = pre;
      
      // -- compute post-image of all PHI nodes in dst
      for(BasicBlock::const_iterator j = bb->begin(),
	    je = bb->end();j != je;++j) 
	{  
	  //-- for PHI node
	  if(const PHINode *phi = dyn_cast<PHINode>(&(*j)))
	    {
	      Value *thisVal = phi->getIncomingValueForBlock (src);

	      if (isBoolType(thisVal->getType()))
		{
		  if (const ConstantInt *k = dyn_cast<ConstantInt> (thisVal))
		    {
		      res = domain.forget (res, phi);
		      // -- zero is false
		      res = domain.meet (res, k->isZero () ? 
					 domain.assumeFalse (phi) :
					 domain.assumeTrue (phi));
		    }
		  else
		    res = domain.alphaPost (res, phi, thisVal, 1, 0);
		}
	      else if (isIntNumber (thisVal))
		res = domain.alphaPost (res, phi, toMpz (thisVal));
	      else 
		res = domain.alphaPost (res, phi, thisVal, 1, 0);
	      
	    }
	  else  // -- first non-phi node, no more PHI nodes after this one
	    break;
	}
      
      return res;
    }


    /** Post image of an instruction in an edge on the way to dst */
    V postImage (const V pre,
		 const Instruction *inst,
		 const BasicBlock *dst) const
    {
      // -- compare
      if(const CmpInst *ci = dyn_cast<CmpInst>(inst)) 
	return processCmpInst(pre, ci);
      // -- branch
      else if(const BranchInst *bi = dyn_cast<BranchInst>(inst)) 
	{
	  V res =  processBranchInst(pre, bi, dst);
	  return res;
	}
      
      // -- select 
      else if(const SelectInst *ci = dyn_cast<SelectInst>(inst))
	return processSelectInst(pre, ci);

      // -- PHI node -- do nothing since they have already been processed
      // -- previously when computing pre
      else if(isa<PHINode>(inst)) return pre;

      // -- extract element
      else if(isa<ExtractElementInst>(inst)) return pre;

      // -- binary operation
      else if(const BinaryOperator *bo = dyn_cast<BinaryOperator>(inst)) 
	return processBinaryOp (pre, bo);
      // -- load -- treated like a NONDET assignment
      else if(const LoadInst *li = dyn_cast<LoadInst>(inst)) 
	return domain.forget (pre,li);
      
      //store -- treated like a NOP
      else if(dyn_cast<StoreInst>(inst)) ;
      
      //return -- treated like a NOP
      else if(isa<ReturnInst>(inst)) {}

      //allocation
      else if(isa<AllocaInst>(inst)) {}
      //call
      else if(const CallInst *ci = dyn_cast<CallInst>(inst))
	return domain.alphaPost (pre, ci);

      //cast
      else if(const CastInst *ci = dyn_cast<CastInst>(inst)) 
	return processCastInst(pre, ci);
      //switch
      else if(isa<SwitchInst>(inst)) {}
      //unreachable
      else if(isa<UnreachableInst>(inst)) {}
      //get element ptr
      else if(isa<GetElementPtrInst>(inst)) {}
      
      return pre;
    }


    V processBinaryOp (V pre, const BinaryOperator *bo) const
    {
      const Value *lhs = bo;       
      Value *op0 = bo->getOperand(0);
      Value *op1 = bo->getOperand(1);

      switch(bo->getOpcode()) 
	{
	      
	case BinaryOperator::Add:
	  if (isIntNumber (op0) && isIntNumber (op1))
	    return domain.alphaPost (pre, lhs, toMpz (op0) + toMpz (op1));
	  
	  
	  if (isIntNumber (op0))
	    return domain.alphaPost (pre, lhs, op1, 1, toMpz (op0));

	  if (isIntNumber (op1))
	    return domain.alphaPost (pre, lhs, op0, 1, toMpz (op1));

	  return domain.alphaPost (pre, lhs, op0, 1, op1, 1);

	case BinaryOperator::Sub:
	  if (isIntNumber (op0) && isIntNumber (op1))
	    return domain.alphaPost (pre, lhs, toMpz (op0) - toMpz (op1));
	  
	  if (isIntNumber (op0))
	    return domain.alphaPost (pre, lhs, op1, -1, toMpz (op0));

	  if (isIntNumber (op1))
	    return domain.alphaPost (pre, lhs, op0, 1, -toMpz (op1));

	  return domain.alphaPost (pre, lhs, op0, 1, op1, -1);

	case BinaryOperator::Mul:
	  if (isIntNumber (op0) && isIntNumber (op1))
	    return domain.alphaPost (pre, lhs, toMpz (op0) * toMpz (op1));

	  if (isIntNumber (op0))
	    return domain.alphaPost (pre, lhs, op1, toMpz (op0), 0);

	  if (isIntNumber (op1))
	    return domain.alphaPost (pre, lhs, op0, toMpz (op1), 0);

	  return domain.forget (pre, lhs);
	  
	case BinaryOperator::And:
	case BinaryOperator::Or:
	case BinaryOperator::Xor:
	  return domain.forget (pre, lhs);
	default:
	  return domain.forget (pre, lhs);
	}
    }


    std::pair<V, V> getConditional (const Instruction* inst)
      const
    {
      if(const CmpInst *ci = dyn_cast<CmpInst>(inst)) 
	return domain.alphaCmp (ci);

       
      V cond = getConditionalHelper(inst, false);
      V ncond = getConditionalHelper(inst, true);

      return std::pair<V, V>(cond, ncond);
    }
      
    V getConditionalHelper (const Instruction* inst, bool neg) const
    {

      if (domain.isVar (inst))
	return neg ? domain.assumeFalse (inst) : domain.assumeTrue (inst);
      
      //errs () << "\nHelper: Computing cond of " << *instr << "\n";	
      if(const CmpInst *ci = dyn_cast<CmpInst>(inst))
	return neg ? domain.alphaCmp (ci).second : domain.alphaCmp (ci).first;

      // --  if it's a phi-node, then it's boolean
      if(const PHINode *pn = dyn_cast<PHINode>(inst)) 
	{
	  assert (isBoolType(pn->getType()));
	  return neg ? domain.assumeFalse (inst) : domain.assumeTrue (inst);
	}
	
      int op = inst->getOpcode();

      assert (op == BinaryOperator::And || 
	      op == BinaryOperator::Or || 
	      BinaryOperator::Xor);

      const Value* v1 = inst->getOperand(0);
      const Value* v2 = inst->getOperand(1);
      

      assert (isBoolType (v1->getType ()));
      assert (isBoolType (v2->getType ()));


      V op1;
      // -- if operands are constants, get their values
      // -- otherwise, resolve in the environment
      if (const ConstantInt *k = dyn_cast<ConstantInt>(v1))
	{
	  if (neg)
	    op1 = k->isZero () ? domain.top () : domain.bot ();
	  else
	    op1 = k->isZero () ? domain.bot () : domain.top ();
	}
      else
	{
	  const Instruction* i1 = dyn_cast<Instruction>(v1);
	  assert (i1 != NULL);
	  op1 = getConditionalHelper(i1, neg);
	}

      V op2;
      if (const ConstantInt *k = dyn_cast<ConstantInt>(v2))
	{
	  if (neg)
	    op2 = k->isZero () ? domain.top () : domain.bot ();
	  else
	    op2 = k->isZero () ? domain.bot () : domain.top ();
	}
      else
	{
	  const Instruction* i2 = dyn_cast<Instruction>(v2);
	  assert (i2 != NULL);
	  op2 = getConditionalHelper(i2, neg);
	}


      switch(op)
	{
	case BinaryOperator::And:
	  return neg ? domain.join (op1, op2) : domain.meet (op1, op2);
	case BinaryOperator::Or:
	  return !neg ? domain.join (op1, op2) : domain.meet (op1, op2);
	case BinaryOperator::Xor:
	  //return neg ? domain.eq (op1, op2) : domain.neq (op1, op2);
	  return domain.top ();
	default:
	  assert(0);
        }
    }
      
    V processSelectInst(const V pre, const SelectInst* inst) const
    {
      const Value *c = inst->getCondition();
      const Value *v1 = inst->getTrueValue();
      const Value *v2 = inst->getFalseValue();
        
      const Instruction* ci = dyn_cast<Instruction>(c);
      assert(ci);
      std::pair<V, V> cond = getConditional (ci);
	
	
      // -- then branch;
      V val1 = domain.meet (pre, domain.assumeTrue (c));

      V thenB1 = domain.meet(val1, cond.first);

      // XXX AG: Make it be the same a in postPhi. If v1 is a Boolean
      // constant, then use assumeTrue/assumeFalse instead of alphaPost.
      V thenB2;
      if (isIntNumber (v1))
	{
	  if (isBoolType (inst->getType ()))
	    thenB2 = domain.meet (thenB1, toMpz(v1) == 0 ?
				  domain.assumeFalse (inst) :
				  domain.assumeTrue (inst));
	  else
	    thenB2 = domain.alphaPost (thenB1, inst, toMpz (v1));
	}
      
      else 
	thenB2 = domain.alphaPost (thenB1, inst, v1, 1, 0);

      // -- else branch
      V val2 = domain.meet (pre, domain.assumeFalse (c));
  
      V elseB1 = domain.meet(val2, cond.second);
      V elseB2;
      if (isIntNumber (v2))
	{
	  if (isBoolType (inst->getType ()))
	    elseB2 = domain.meet (elseB1, toMpz(v2) == 0 ?
				  domain.assumeFalse (inst) :
				  domain.assumeTrue (inst));
	  else
	    elseB2 = domain.alphaPost (elseB1, inst, toMpz (v2));
	}
      else 
	elseB2 = domain.alphaPost (elseB1, inst, v2, 1, 0);
         
      // XXX AG: a lot of code duplication. Factor out into helper functions
      return domain.join(thenB2, elseB2);
    }
      
    V processCmpInst(const V pre, const CmpInst *inst) const
    {
      //-- abstract the compare condition and its negation
      std::pair<V,V> cond = domain.alphaCmp (inst);

      //-- propagate along then edge
      V postThen = domain.meet (domain.forget (pre, inst),
				domain.assumeTrue (inst));
      postThen = domain.meet (postThen, cond.first);

      //-- propagate along else edge
      V postElse = domain.meet(domain.forget (pre,inst),
					domain.assumeFalse (inst));
      postElse = domain.meet(postElse, cond.second);      


      //-- join results
      return domain.join(postThen, postElse);
    }

    V processBranchInst (const V pre, const BranchInst *inst,
			 const BasicBlock *dst) const
    {
      if(!inst->isConditional()) 
	{
	  assert (inst->getSuccessor (0) == dst);
	  return pre;
	}

      //conditional branch
      BasicBlock *BB0 = inst->getSuccessor(0);
      BasicBlock *BB1 = inst->getSuccessor(1);
	      
      //get the branch conditon
      Value *cond = inst->getCondition();

      // get constraints on the branch condition
      std::pair<V,V> constr(domain.top(), domain.top());  
      
      if (const BinaryOperator *bi = dyn_cast<BinaryOperator> (cond))
	{
	  if (!domain.isVar (bi)) constr = getConditional (bi);
	}
      
      else if (const Instruction *ci = dyn_cast<Instruction> (cond))
	{
	  // -- if we are not tracking ci explicitly, get the formula
	  // -- corresponding to it
	  if (!domain.isVar (ci)) constr = getConditional (ci);
	}
      
      else if (const Constant *cst = dyn_cast<Constant> (cond))
	// must be a constant 
	{
	  if (cst->isNullValue ())
	    constr = std::pair<V,V> (domain.bot(), domain.top());
	  else
	    constr = std::pair<V,V> (domain.top(), domain.bot());
	}
      else
	assert (0 && "unreachable");

      // -- then branch -- assume that the branch condition is TRUE
      if (BB0 == dst) 
	{
	  V tmp = domain.meet (pre, domain.assumeTrue (cond)); 
	  return domain.meet (tmp, constr.first);     
	}

      // -- else branch -- assume that the branch condition is FALSE
      {
	assert (BB1 == dst);
	V tmp = domain.meet (pre, domain.assumeFalse (cond));
	tmp = domain.meet (tmp, constr.second);
	return tmp;
      }
    }

    V processCastInst (const V pre, const CastInst *inst) const
    {
      return domain.alphaPost (pre, inst, inst->getOperand (0), 1, 0);
    }
  };
}

#endif
