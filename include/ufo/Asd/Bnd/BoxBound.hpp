#ifndef __BOX_BOUND_HPP_
#define __BOX_BOUND_HPP_

#include <boost/algorithm/string/predicate.hpp>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/optional.hpp>
#include <iostream>
#include "ufo/Asd/Interval.hpp"
#include "ufo/Smt/ExprZ3.hpp"
#include "ufo/Asd/Apron/ExprApron.hpp"
#include "ufo/Asd/Bnd/Bound.hpp"

using namespace expr;
using namespace expr::op;
using namespace boost;
using namespace std;

namespace ufo
{
  /**
   * BoxBound.
   * Bound an array of variables together. It's an alternative to VarBound.
   */
  template <typename AP>
  class BoxBound : public Bound
  {
  private:  
    ExprFactory &efac; // Expression Factory
    const ExprSet &varbase; // Variables to be bounded
    ExprZ3 z3; // Z3 object
    ExprSet realVars;
    ExprSet boolVars;
    AP ap;
    AbstractState state; 

  public:
    BoxBound (ExprFactory &_e, const ExprSet &_v) :
      efac(_e), varbase(_v), z3(_e, StringMap (), true), 
      realVars(filterVars(varbase, true)),
      boolVars(filterVars(varbase, false)),
      ap(efac, boolVars, realVars),
      state(ap.bot(CutPointPtr())) {} 

    ~BoxBound () {}

    /**
     * boundAll. 
     * param[in] formula phi.
     */
    void boundAll (Expr phi)
    {
      Stats::resume ("boxbound");
      int count = 1;
      z3.assertExpr (phi);

      while (z3.solve())
        { 
          if (count % ufocl::BOUND_WIDEN != 0)
          {
            join (getModelState()); 
            Stats::count ("boxbound.join");
          }  
          else
          {
            widen (getModelState());
            Stats::count ("boxbound.widen");
          }

          ++count;

          z3.assertExpr (mk<NEG> (gamma()));
          //errs () << "GAMMA: " << *(gamma()) << "\n";
        }
      Stats::stop ("boxbound");
    }

    /**
     * getModelState
     * update work to represent the new point. 
     */
    AbstractState getModelState ()
    {
      ExprMap bmodel, rmodel;
      foreach (Expr _v, boolVars)
        {
          bmodel[_v] = z3.getModelValue (_v);
        }
      foreach (Expr _v, realVars)
        {
          rmodel[_v] = z3.getModelValue (_v);
        }
      return ap.point2state (bmodel, rmodel);
    }

    /**
     * Override [] operator.
     * Provide access to variable bounds.
     * param[in] variable var
     */
    Interval operator [] (Expr var)
    {
      return ap.getBound (var, state);
    }
 
    /**
     * join. join new point with the current range/box.
     * param[in] new point
     * side effect: remove top/unbounded variables from the work
     * list.
     */
    void join (AbstractState p_state)
    {
      state = ap.join (CutPointPtr(), state, p_state); 
    }

    /**
     * widen. apply widen to the current range/box after joinning with
     * the new point.
     * param[in] new point
     * side effect: remove
     * top/unbounded variables from the work list.
     */
    void widen (AbstractState p_state)
    {
      AbstractState old = ap.copy (state);
      join (p_state);
      state = ap.widen (CutPointPtr(), old, state);
    }

    /**
     * gamma function.
     * return Expr representation of the current box.
     */
    Expr gamma ()
    {
      return ap.gamma (CutPointPtr(), state);
    }
  };
}

#endif
