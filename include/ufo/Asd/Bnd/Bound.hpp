#ifndef __BOUND__HPP_
#define __BOUND__HPP_

#include "ufo/ufo.hpp"

namespace ufo
{
  typedef std::map<Expr, Interval> VarBounds;

  /**
   * Parent Class for VarBound, BoxBound and DNFBound
   */
  class Bound
  {
  public:
    virtual ~Bound () {};
    virtual void boundAll (Expr phi) = 0;
    virtual Interval operator [] (Expr var) = 0;

    /**  Test if a var is bool type. */
    bool isBool (Expr var)
    {
      if (isOp<NumericOp>(var)) return false;
      Expr u = var;
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (!isOpX<VALUE>(u)) return true;

      if (isBoolType(getTerm<const Value*>(u)->getType()))
        return true;

      return false;
    }

    /**
     * filterVars.
     * Filter Boolean or real variables.
     * param[in] const ExprSet &vars. variables to be filtered.
     * param[in] bool isReal. Filter real valued variables.
     * return ExprSet.
     */
    template <typename R>
    ExprSet filterVars (const R &vars, bool isReal)
    {
      ExprSet res;
      foreach (Expr v, vars)
      {
        if ((isBool (v) && !isReal)
            || (!isBool(v) && isReal))
          res.insert(v);          
        else continue;
      }
      return res;
    }
    
    /**
     * Filter collecting linear constraints
     */
    struct ConsFilter
    {
      bool operator() (Expr v) const { return isOp<ComparissonOp> (v); }
    };

    /**
     * Filter collecting evaluated variables (VARIANT)
     */
    struct VarsFilter
    {
      bool operator() (Expr v) const { return isOpX<VARIANT> (v); }
    };

    /**
     * Filter collecting bind variables
     */
    struct BindFilter
    {
      bool operator() (Expr v) const { return isOpX<BIND> (v); }
    };

    /**
     * Filter collecting literals:
     * Including variables, terms and bind variables.
     */
    struct LiteralFilter
    {
      bool operator() (Expr v) const
      {
        if (isOpX<NEG> (v)) return true;
        
        return (isOpX<BIND> (v) || isOp<ComparissonOp> (v)
                || isOpX<VARIANT> (v));
      }
    };

    /** This visitor close a formula */
    struct CloseFormulaVisit
    {
      CloseFormulaVisit () {}

      VisitAction operator() (Expr exp)
      {
        if (isOpX<NEG>(exp))
        {
          Expr e = exp->left();
          // Flip operators
          Expr res;
          if (isOpX<LEQ>(e))
            res = mk<NEG>(mk<LT>(e->left(), e->right()));
          if (isOpX<GEQ>(e))
            res = mk<NEG>(mk<GT>(e->left(), e->right()));
          if (isOpX<GT>(e))
            res = exp;
          if (isOpX<LT>(e))
            res = exp;
          if (isOpX<EQ>(e))
            res = mk<TRUE>(e->efac());
          if (isOpX<NEQ>(e))
            res = mk<EQ>(e->left(), e->right());

          if (res)
            return VisitAction::changeTo(res);
        }
        if (isOpX<GT>(exp)) 
          return VisitAction::changeTo(mk<GEQ>(exp->left(),exp->right()));
        if (isOpX<LT>(exp)) 
          return VisitAction::changeTo(mk<LEQ>(exp->left(),exp->right()));
        if (isOpX<NEQ>(exp))
          return VisitAction::changeTo(mk<TRUE>(exp->efac()));

        return VisitAction::doKids();
      }
    };
    
    /**
     * This simplifier flatten the formula.
     * Change IFF, IMPL, XOR, ITE to AND, OR
     */
    struct DeepSimplifier
    {
      ExprFactory &efac;
      DeepSimplifier (ExprFactory &_e) : efac(_e) {}

      VisitAction operator () (Expr exp)
      {
        // (a && b) || (!a && !b)
        if (isOpX<IFF> (exp))
          return VisitAction::changeDoKids (
            boolop::lor (mk<AND> (exp->left(), exp->right()),
                    boolop::land
                    (boolop::lneg(exp->left()), boolop::lneg(exp->right()))));

        // !a || b
        if (isOpX<IMPL> (exp))
          return VisitAction::changeDoKids (
            boolop::lor (boolop::lneg (exp->left()), exp->right()));

        // a&&!b || !a&&b
        if (isOpX<XOR> (exp))
          return VisitAction::changeDoKids (
            boolop::lor (boolop::land (exp->left(), boolop::lneg(exp->right())),
                    boolop::land (boolop::lneg(exp->left()), exp->right())));
              
        if (isOp<ComparissonOp> (exp))
        {
          // x = ITE(b, y, z)
          // b/\(x=y) || !b/\(x=z)
          if (isOpX<EQ>(exp) && isOpX<ITE> (exp->right()))
          {
            Expr x = exp->left();
            Expr b = exp->right()->left();
            Expr y = exp->right()->right();
            Expr z = *((exp->right()->args_begin())+2);
              
            return VisitAction::changeTo (
              boolop::lor(
                boolop::land (b, mk<EQ>(x,y)),
                boolop::land (boolop::lneg(b), mk<EQ>(x,z))
                )
              );
          }
        }

        return VisitAction::doKids();
      }
    };

    /** Deep Simplify formula */
    Expr deepSimp (Expr e)
    {
      DeepSimplifier ds (e->efac());
      e = dagVisit (ds, e);
      e = boolop::nnf (e);
      return e;
    }

    /** Flip operator */
    Expr flip (Expr c)
    {
      assert (isOp<ComparissonOp> (c));
      Expr left = c->left();
      Expr right = c->right();
      if (isOpX<EQ> (c))
        return mk<NEQ> (left, right);
      else if (isOpX<NEQ> (c))
        return mk<EQ> (left, right);
      else if (isOpX<GT> (c))
        return mk<LEQ> (left, right);
      else if (isOpX<LT> (c))
        return mk<GEQ> (left, right);
      else if (isOpX<GEQ> (c))
        return mk<LT> (left, right);
      else if (isOpX<LEQ> (c))
        return mk<GT> (left, right);
      else
        assert (0 && "Unreachable");
    }
    
    /** Close the formula */
    Expr closeFormula(Expr e)
    {
      // Convert to Negative Normal Form
      e = boolop::nnf (e);
      // Close operators
      CloseFormulaVisit cv;
      e = dagVisit (cv,e);
      return e;
    }

    /** Collect literals from formula */
    ExprSet collectLiterals (Expr formula)
    {
      ExprSet literals;
      filter (formula, LiteralFilter(), inserter (literals, literals.begin()));
      return literals;
    }
    
    /** Collect variables from formula */
    ExprSet collectVars (Expr formula)
    {
      ExprSet vars;
      filter (formula, VarsFilter (), std::inserter(vars, vars.begin()));
      return vars;
    }

    /**
     * boundBool. Bound Boolean variables.
     * param[in] ExprZ3 &z3.
     * param[in] Expr phi.
     * param[in] ExprSet &vars. variables.
     * param[out] VarBounds &res. a map to store the results.
     */
    void boundBool (Expr phi, ExprSet &vars, VarBounds &res)
    {
      ExprZ3 z3 (phi->efac());
      z3.assertExpr (phi);
      foreach (Expr _v, vars)
      {
        Interval r = Interval::bot();
        z3.push();
        z3.assertExpr(_v);
        if (z3.solve()) r.second = posInf();
        z3.pop();
        z3.push();
        z3.assertExpr(mk<NEG>(_v));
        if (z3.solve()) r.first = negInf();
        z3.pop();

        //assert (res.count(_v) > 0);
        res[_v] = r;
      }
    }
  };
}

#endif
