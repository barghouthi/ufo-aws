/** Intervals over extended real numbers and helper functions */
#ifndef __INTERVAL__HPP_
#define __INTERVAL__HPP_

#include <gmpxx.h>
#include "ufo/ufo.hpp"
#include "ufo/Expr.hpp"

namespace ufo
{
  /** infinity is  x/0, where x != 0 */
  inline bool isInf (mpq_class k)
  {
    return sgn (k.get_den ()) == 0 && sgn (k.get_num ()) != 0;
  }

  /** wrong mpq value 0/0 **/
  inline bool isWrong (mpq_class k)
  {
    return sgn (k.get_den ()) == 0 && sgn (k.get_num()) == 0;
  }

  /** positive infinity is x/0, where x > 0 */
  inline bool isPosInf (mpq_class k)
  {
    return sgn (k.get_den ()) == 0 && sgn (k.get_num ()) > 0;
  }

  /** negative infinity is x/0, where x < 0 */
  inline bool isNegInf (mpq_class k)
  {
    return sgn (k.get_den ()) == 0 && sgn (k.get_num ()) < 0;
  }

  /** construct positive infinity */
  inline mpq_class posInf () { return mpq_class (1, 0); }
  /** construct negative infinity */
  inline mpq_class negInf () { return mpq_class (-1, 0); }

  /** leq that knows about infinity */
  inline bool leq (mpq_class x, mpq_class y)
  {
    if (isNegInf (x)) return true;
    if (isPosInf (y)) return true;

    if (isInf (x) || isInf (y))
      return (isNegInf (x) && isNegInf (y)) || (isPosInf (x) && isPosInf (y));

    return x <= y;
  }

  inline bool eq (mpq_class x, mpq_class y)
  {
    if (isInf (x) || isInf (y))
      return (isNegInf (x) && isNegInf (y)) || (isPosInf (x) && isPosInf (y));
    return x == y;
  }

  inline mpq_class max (mpq_class x, mpq_class y) 
  { 
    //assert(!isWrong(x) && !isWrong(y)); 
    return leq (x, y) ? y : x; 
  }

  inline mpq_class min (mpq_class x, mpq_class y) 
  {
    //assert(!isWrong(x) && !isWrong(y));
    return leq (x, y) ? x : y; 
  }



  /*******************************************************************/
  //an interval is a pair of IntVals -- minimum and maximum
  /*******************************************************************/
  class Interval : public std::pair<mpq_class,mpq_class>
  {
  public:
    Interval() : std::pair<mpq_class,mpq_class>(posInf (), negInf ()) {}

    Interval (const mpq_class &min, const mpq_class &max) : 
      std::pair<mpq_class,mpq_class>(min, max) 
    {
      //if (!isBot ())
      //assert (!isPosInf (min));
      //assert (!isNegInf (max));
      //errs() << min << ", " << max << "\n";
      assert (!isWrong(min));
      assert (!isWrong(max));
    }

    //check eq -- if this is equal to the argument
    bool operator == (const Interval &rhs) const 
    {
      return eq (first, rhs.first) && eq (second, rhs.second);
    }

    //check neq -- if this is not equal to the argument
    bool operator != (const Interval &rhs) const 
    {
      return !(*this == rhs);
    }

    //check leq -- if this is contained in the argument
    bool operator <= (const Interval &rhs) const 
    {
      return leq (rhs.first, first) && leq (second, rhs.second);
    }

    //meet
    Interval meet(const Interval &rhs) const 
    {
      //check if the two intervals have non-empty intersection
      if (leq (rhs.first, second) && leq (first, rhs.second))
        return Interval (max (first, rhs.first), min (second, rhs.second));

      return bot ();
    }

    //join
    Interval join(const Interval &rhs) const 
    {
      return Interval (min (first, rhs.first), max (second, rhs.second));
    }

    //widen
    Interval widen(const Interval &rhs) const 
    {
      if (isBot ()) return rhs;
      if (*this == rhs) return *this;

      assert(*this <= rhs);      
      return Interval (eq (first, rhs.first) ? first : negInf (),
                       eq (second, rhs.second) ? second : posInf ());
    }

    Expr gamma (Expr x)
    {
      if (isBot ()) return mk<FALSE> (x->efac ());
      if (isTop ()) return mk<TRUE> (x->efac ());

      if (isTrueI(x)) return x; 
      if (isFalseI(x)) return mk<NEG>(x);

      Expr res = mk<TRUE> (x->efac ());
      if (!isNegInf (first)) 
        res = boolop::land (res, 
                            mk<GEQ> (x, mkTerm (first, x->efac ())));

      if (!isPosInf (second)) 
        res = boolop::land (res, 
                            mk<LEQ> (x, mkTerm (second, x->efac ())));

      return res;
    }

    bool isTrueI (Expr x) const
    {
      return isBool(x) && isPosInf(second) && isPosInf(first);//!isNegInf(first); 
    }

    bool isFalseI (Expr x) const
    {
      return isBool(x) && isNegInf(first) && isNegInf(second);//!isPosInf(second);
    } 

    //return the top value (-inf,+inf)
    static Interval top() { return Interval(negInf(), posInf());}

    //return the bottom value (+inf,-inf)
    static Interval bot() { return Interval(posInf (), negInf ()); }

    bool isBot () const
    {
      //return isPosInf (first) && isNegInf (second);
      return !leq(first, second);
    }
    bool isTop () const
    {
      return isNegInf (first) && isPosInf (second);
    }

  private:
    bool isBool (Expr var) const
    {
      if (!(isOpX<VALUE>(var) ||
            isOpX<VARIANT>(var)))
        return false;
      Expr u = var;
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (isOpX<VARIANT>(u)) u = variant::mainVariant(u);
      if (!isOpX<VALUE>(u)) return true;

      if (isBoolType(getTerm<const Value*>(u)->getType()))
        return true;

      return false;
    }
  };
}


#endif 
