/** Commmand line options for UFO */
#include "ufo/ufo.hpp"

namespace ufocl
{
  using namespace ufo;
  using namespace llvm;

  /** command line options */

  cl::opt<string>
  TRACE_FILENAME ("ufo-trace",
		  cl::desc ("Trace / Counterexample file name"),
		  cl::value_desc("filename"));
  
  cl::opt<unsigned> 
  MIN_UNROLL ("ufo-min-unroll",
	      cl::desc("Minum number of unrollings before cover check"),
	      cl::init (0)
    );

  cl::opt<unsigned> 
  UNROLL_STEP ("ufo-unroll-step",
	       cl::desc("Number of unrollings per EXPAND step"),
	       cl::init (1)
    );

  cl::opt<enum Post> 
  UFO_POST ("ufo-post", 
	    cl::desc("The type of POST to use"),
	    cl::values (clEnumVal(BPRED, "Boolean Pred. Abs."),
			clEnumVal(BOUND, "Bound var domain by Yi"),
			clEnumVal(CPRED, "Cartesian Pred. Abs."),
			clEnumVal(BOXES,  "Boxes abstract domain"),
			clEnumVal(BOX,  "Box abstract domain"),
			clEnumVal(BOXCPRED,  "Box + Cart PA"),
			clEnumVal(BOXBPRED,  "Box + Bool PA"),
			clEnumVal(BOXESCPRED,  "Boxes + Cart PA"),
			clEnumVal(BOXESBPRED,  "Boxes + Bool PA"),
			clEnumVal(BOX_LIGHT_CPRED,  "Box abstract domain"),
			clEnumVal(BOX_LIGHT_BPRED,  "Box abstract domain"),
			clEnumVal(NONE,  "No Predicates"),
			clEnumValEnd),
	    cl::init (NONE)
    );

  cl::opt<enum RefLabels> 
  UFO_LBL ("ufo-ref-labels", 
           cl::desc("Node labels for refinement"),
           cl::values (
             clEnumVal(NODES,  "Use node labels"),
             clEnumVal(INV,  "Use invariant"),
             clEnumValEnd),
           cl::init (NODES)
    );


  cl::opt<bool>
  UFO_DEBUG ("ufo-debug",
	     cl::Hidden,
	     cl::desc ("enable UFO debugging"),
	     cl::init (false)
    );

  cl::opt<enum Validate>
  UFO_VALIDATE ("ufo-validate",
		cl::desc ("Granularity of proof/cex validation\n"),
		cl::values (clEnumVal(NEVER, "No validation (default)"),
			    clEnumVal(LAST, "Validate final result"),
			    clEnumVal(ITER, "Validate after every iteration"),
			    clEnumVal(POST, "Validate after every POST"),
			    clEnumValEnd),
		cl::init (NEVER)
    );
  

  cl::opt<bool>
  UFO_TRACK_PTR ("ufo-track-ptr",
		 cl::desc ("Track pointer addresses\n"),
		 cl::init (false));

  cl::opt<bool>
  UFO_OD_ONLY ("ufo-od-only",
	       cl::desc ("Only use Over-Approximation\n"),
	       cl::init (false)
    );

  cl::opt<unsigned>
  WIDEN_STEP ("ufo-widen-step",
	      cl::desc ("Number of steps before widening\n"),
	      cl::init (1)
    );

  cl::opt<enum Refine>
  INC_REFINE ("ufo-increfine",
	      cl::desc ("Incremental refine\n"),
	      cl::values (clEnumVal(REF0, "Normal refinement"),
			  clEnumVal(REF1, "Incremental refinement 1 "
				    "-- requires ufo-combine"),
			  clEnumVal(REF2, "Incremental refinement 2 "
				    "-- requires ufo-combine"),
			  clEnumVal(REF3, "Incremental refinement 3"),
			  clEnumVal(REF4, "Refine with assumptions"),
			  clEnumVal(REF5, "Dag Itp Refiner"),
			  clEnumValEnd),
	      cl::init (REF0)
    );
  
  
  
  cl::opt<bool>
  CONS_REFINE ("ufo-consrefine",
	       cl::desc ("Constrain refinement with state formulas\n"),
	       cl::init (false)
    );
  

  cl::opt<bool>
  COMBINE ("ufo-combine",
	   cl::desc ("Combine box and interp labels\n"),
	   cl::init (false)
    );
  
  cl::opt<bool>
  UFO_CONJOIN_LABELS ("ufo-conjoin",
		      cl::desc ("Conjoin labels from different iterations\n"),
		      cl::init (false)
    );
  
  cl::opt<bool> USE_INTS ("ufo-use-ints",
			  cl::desc ("Use integers for untyped numerals\n"),
			  cl::init (false));
  
  cl::opt<bool>
  UFO_CONJOIN_OLD_LABEL 
  ("ufo-conjoin-old-labels",
   cl::desc ("Conjoin old refinement labels from different iterations\n"),
   cl::init (false)
    );
  

  cl::opt<bool>
  FALSE_EDGES ("ufo-false-edges",
	       cl::desc ("Enable false edges\n"),
	       cl::init (false)
    );
  

  cl::opt<bool>
  UFO_DVO ("ufo-dvo",
	   cl::desc ("Dynamic Variable Reordering for Box(es)\n"),
	   cl::init (false)
    );
  
  cl::opt<bool>
  UFO_SIMPLIFY ("ufo-simplify",
		cl::desc ("Don't simplify interpolants\n"),
		cl::init (true)
    );



  cl::opt<enum Cover>
  COVER ("ufo-cover", 
	 cl::desc ("Cover strategy\n"),
	 cl::values (clEnumVal(ADOM, "Abstract domain cover"),
		     clEnumVal(IMP, "SMT-based implication"),
		     clEnumVal(GLOBAL, "GLOBAL cover using Z3"),
		     clEnumValEnd),
	 cl::init (GLOBAL)
    ); 
  
  cl::opt<enum Widen>
  WIDEN ("ufo-widen",
	 cl::desc ("Widening strategy\n"),
	 cl::values (clEnumVal(WID0, "Widen against maximal values"),
		     clEnumVal(WID1, "Widen against all heads"),
		     clEnumValEnd),
	 cl::init (WID0)
    );

  cl::opt<enum Hints>
  HINTS ("ufo-hints",
         cl::desc ("Refinement hits strategy\n"),
         cl::values (
           clEnumVal (HIN0, "Extract variables from hints"),
           clEnumVal (HIN1, "Extract terms from hints"),
           clEnumVal (HIN2, "Extract variables and terms from hints"),
           clEnumValEnd),
         cl::init (HIN0)
    );

  cl::opt<enum BoundAlgo>
  UFO_BOUND ("ufo-bound",
             cl::desc ("Bound domain algorithm\n"),
             cl::values (clEnumVal(BOU0, "VarBound algorithm"),
                         clEnumVal (BOU1, "BoxBound algorithm"),
                         clEnumVal (BOU2, "DNFBound algorithm"),
                         clEnumValEnd),
             cl::init (BOU0)
    );

  cl::opt<unsigned>
  BOUND_WIDEN ("ufo-bound-widen",
               cl::desc ("Steps before applying widen in Bound algorithms\n"),
               cl::init (1)
    );

  cl::opt<enum ApronLib>
  APLIB ("ufo-aplib",
         cl::desc ("Apron abstract domain library\n"),
         cl::values (
           clEnumVal(ABOX, "Apron Box domain"),
           clEnumVal (AOCT, "Apron Octagon domain"),
           clEnumVal (APK, "Apron NewPolka domain"),
           clEnumValEnd),
         cl::init (ABOX)
    );
}

