#include "ufo/InitializePasses.h"

#include <iostream>
#include <typeinfo>
#include <string>
#include <set>
#include <map>
#include <queue>
#include <stack>
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Instructions.h"
#include "llvm/Constants.h"
#include "llvm/Support/CFG.h"
#include "ufo/Cpg/Topo.hpp"
#include "llvm/Module.h"
using namespace llvm;


// NOT USED
// static cl::opt<bool> 
// TopoOpt("TopoOpt",
// 	cl::desc("topological ordering of basic blocks"),
// 	cl::init(false));
	
	
	
void Topo::findBackEdges(Function &F)
{
  //NOTE: perhaps computing cutpoints can be incorporated here
  //NOTE: sets may not be efficient here on big examples.

  // -- entry block
  BasicBlock &init = F.getEntryBlock();	

  // -- visited blocks in DFS
  std::set<BasicBlock*> visited;			

  // -- stack containing current path in DFS
  //NOTE: need to specify threshold, not sure what a good number would be
  SmallVector<BasicBlock*,1000> pathSt;
  pathSt.push_back(&init);			

  // XXX It's ok to linear search pathSt. Will probably be more
  // XXX efficient overall.

  // -- set for access of stack elements
  std::set<BasicBlock*> pathBlocks;		
  pathBlocks.insert(&init);
		

  // DFS
  while (!pathSt.empty())
    {
      // -- signals if new nodes added to stack
      bool found = false;						
      
      BasicBlock* node = pathSt.back();

      // XXX this is moderatelly inefficient since it always iterates
      // XXX over all the children, even if we saw most of them
      // XXX already.  Recursive implementation would solve this
      // XXX problem. Alternatively, the stack should keep both the node
      // XXX and current SI
      
      // -- iterate over children
      for (succ_iterator SI = succ_begin(node), E = succ_end(node); 
	   SI != E; ++SI) 
	{
	  BasicBlock* child = *SI;

	  // -- if child is on stack, then there's a backedge
	  if (pathBlocks.count (child) > 0)
	    {
	      backEdges[&F][node].insert (child);
	      continue;
	    }
	  //if child is not on stack and hasn't been visited
	  //continue exploration from there
	  else if (visited.count (child) == 0)
	    {
	      pathSt.push_back(child);
	      pathBlocks.insert(child);
	      found = true;
	      break;
	    }

	}
    
      // if no child added to stack, then current node has been exhausted.
      // pop it off the stack and declare it visited.
      if (!found)
	{
	  pathBlocks.erase (pathSt.back());
	  pathSt.pop_back ();
	  visited.insert (node);
	}
    }
  return;
}

bool Topo::runOnModule (Module &M) 
{ 
    errs () << "\n== Running Topo pass ==\n;";
    Function& F = *(M.getFunction("main"));
    
    // -- make sure that there's one return statement
    //UnifyFunctionExitNodes& u =
    getAnalysis<UnifyFunctionExitNodes>(F);
    
    // -- compute backedges
    findBackEdges (F);
		
    // -- map to store indegree for each bb
    DenseMap<BasicBlock*,int> inDegMap;
		
    std::queue<BasicBlock*> qu;
		
    // -- iterate through basic blocks and compute indegree
    for (Function::iterator it = F.begin(), e = F.end(); it != e; ++it)
    {
      BasicBlock* node = it;
      int inDeg = 0;
     
      // -- compute indegree, a backedge into the node does not count
      for (pred_iterator PI = pred_begin(node), E = pred_end(node); 
	   PI != E; ++PI) 
	if (!isBackEdge (&F, *PI, node)) inDeg++;

      // -- store 
      inDegMap [node] = inDeg;
      
      // if inDeg=0, then node is added to search queue
      // because it's an initial basic block
      if (inDeg == 0) qu.push (node);
    }
		
    //computes forward topo order
    while (!qu.empty())
    {
      BasicBlock* bb = qu.front ();
      qu.pop();

      topo[&F].push_back (bb);

      //iterate over successors of node
      for (succ_iterator SI = succ_begin(bb), E = succ_end(bb); SI != E; ++SI) 
	{
	  BasicBlock* succ = *SI;
	  if (isBackEdge(&F, bb,succ)) continue;
				
	  // -- decrease indegree of successor
	  // -- in effect, we're removing edges
	  DenseMap<BasicBlock*,int>::iterator it = inDegMap.find (succ);
	  assert (it != inDegMap.end ());

	  // XXX A better way is to store a class with int field in the map
	  // XXX and decrement the field
	  int inDeg = it->second;
	  inDegMap.erase (it);
	  inDegMap[succ] = inDeg - 1;

				
	  //when indegree of successor reaches 0, add to qu to be processesed
	  if (inDeg == 1) qu.push (succ);
	}
      }
    //no blocks missed
    assert ( topo[&F].size () == F.getBasicBlockList ().size ());
  return false;
}
	

char Topo::ID = 0;
//static RegisterPass<Topo> 
//X("Topo", "Order blocks in fwd/bwd topological order.");
INITIALIZE_PASS_BEGIN(Topo, "Topo", 
		      "Order blocks in fwd/bwd topological order.", 
		      false, false)
INITIALIZE_PASS_DEPENDENCY(UnifyFunctionExitNodes)
INITIALIZE_PASS_END(Topo, "Topo", 
		      "Order blocks in fwd/bwd topological order.", 
		      false, false)

