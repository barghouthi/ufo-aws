#ifndef __INC_REFINER_HPP_
#define __INC_REFINER_HPP_

#include <list>

#include "ufo/ufo.hpp"

#include "ufo/Cpg/Lbe.hpp"
#include "ufo/Arg.hpp"

#include "ufo/EdgeComp.hpp"

#include "OldRefiner.hpp"


/** 
 * Incremental Refiner. 
 * 
 * This code is experimental and is not currently maintained.
 */

namespace ufo
{
  class IncRefiner : public OldRefiner
  {
  protected:
    std::vector<Node*> prevOrderedNodes;
    std::map<Node*, Expr> prevNodeExpr;
    std::map<Node*, Expr> prevInterps;
    
  public:
    IncRefiner (ExprFactory &fac, 
		AbstractStateDomain &dom,
		LBE &cpG,
		DominatorTree &dt,
		ExprMSat &msat,
		ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      OldRefiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
    
    virtual void refine ();
  protected:
    void computeInterpolants2(std::vector<Node*>& orderedNodes, 
			      std::map<Node*, int>& node2group,
			      std::set<Node*>& asserted,
			      int initGroup,
			      std::map<int,Environment>& inScope,
			      std::map<Node*, Expr>& node2cons);
    
    int assertInitExpr(std::vector<Node*>& onodes, size_t pos,
	               std::map<Node*, Expr>& node2lbl, 
		       bool incRefine2 = false);
  };
  

  class IncRefiner2 : public IncRefiner
  {
  public:
    IncRefiner2 (ExprFactory &fac, 
		 AbstractStateDomain &dom,
		 LBE &cpG,
		 DominatorTree &dt,
		 ExprMSat &msat,
		 ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      IncRefiner (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
    
    virtual void refine ();
    protected:
    void extendSubgraph(std::set<Node*>& subGraph, 
			std::set<Node*>& reachable, Node* error);
    

  };

  class IncRefiner3 : public IncRefiner2
  {
  public:
    IncRefiner3 (ExprFactory &fac, 
		 AbstractStateDomain &dom,
		 LBE &cpG,
		 DominatorTree &dt,
		 ExprMSat &msat,
		 ARG &a, bool doSimp, NodeExprMap& nodeLabels) :
      IncRefiner2 (fac, dom, cpG, dt, msat, a, doSimp, nodeLabels) {}
    
    virtual void refine ();
  };
}


#endif
